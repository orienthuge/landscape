FROM shu300/php71:latest

WORKDIR /var/www

RUN rm -rf html
ARG version=3.1.5
ENV VERSION $version
RUN git clone -b $VERSION https://github.com/oroinc/crm-application.git html

#USER www-data
WORKDIR /var/www/html

RUN composer self-update
RUN COMPOSER_MEMORY_LIMIT=4G composer install --prefer-dist
#RUN COMPOSER_MEMORY_LIMIT=4G composer require --update-no-dev "hackzilla/password-generator" "^1.4"
#RUN COMPOSER_MEMORY_LIMIT=4G composer install --prefer-dist
RUN chown www-data.www-data ../html -R

RUN apt-get install netcat gnupg -y
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install nodejs -y
