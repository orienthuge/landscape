REPO = orocrm
DOCKER_PATH = docker.senal.club/library
VERSION = 3.1.5

all:
	docker build --build-arg version=${VERSION} -t ${DOCKER_PATH}/${REPO}:${VERSION} .
#docker tag docker.senal.club/mtrust/backend:${version} shu300/php71-orocrm:latest

push:
	docker push ${DOCKER_PATH}/${REPO}:${VERSION}
#docker push shu300/php71-orocrm:latest
