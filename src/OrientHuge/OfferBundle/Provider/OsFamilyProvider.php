<?php

namespace OrientHuge\OfferBundle\Provider;

use OrientHuge\OfferBundle\Model\OsFamily;
use Symfony\Component\Translation\TranslatorInterface;

class OsFamilyProvider
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var array
     */
    protected $choices = array(
        OsFamily::ANDROID   => 'Android',
        OsFamily::IOS => 'iOS',
        OsFamily::WEB => 'Web',
        OsFamily::UNKNOWN => 'Unknown',
    );

    /**
     * @var array
     */
    protected $translatedChoices;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getChoices()
    {
        if (null === $this->translatedChoices) {
            $this->translatedChoices = array();
            foreach ($this->choices as $name => $label) {
                $this->translatedChoices[$name] = $this->translator->trans($label);
            }
        }

        return $this->translatedChoices;
    }

    /**
     * @param string $name
     * @return string
     * @throws \LogicException
     */
    public function getLabelByName($name)
    {
        $choices = $this->getChoices();
        if (!isset($choices[$name])) {
            throw new \LogicException(sprintf('Unknown os family with name "%s"', $name));
        }

        return $choices[$name];
    }
}
