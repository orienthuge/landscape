<?php
namespace OrientHuge\OfferBundle\Form\Type;

use OrientHuge\CoreBundle\Entity\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['required' => true, 'label' => 'Name']);
        $builder->add('codeAlpha2', TextType::class, ['required' => true, 'label' => 'Code Alpha2']);
        $builder->add('codeAlpha3', TextType::class, ['required' => true, 'label' => 'Code Alpha3']);
        $builder->add('codeAlias', TextType::class, ['required' => false, 'label' => 'Code Alias']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'           => Country::class,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oh_country';
    }
}
