<?php

namespace OrientHuge\OfferBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class CountryController
 *
 * @Route("/country")
 * @package OrientHuge\OfferBundle\Controller
 */
class CountryController extends Controller
{
    /**
     * @Route("/", name="oh_country_index")
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'entity_class' => Country::class
        ];
    }

    /**
     * @Route("/create", name="oh_country_create")
     * @AclAncestor("oh_advertiser_create")
     * @Template("OrientHugeOfferBundle:Country:update.html.twig")
     * @return array
     */
    public function createAction()
    {
        return $this->update(new Country());
    }

    /**
     * @Route("/update/{id}", name="oh_country_update", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_update")
     * @Template
     * @param Country $entity
     * @return array
     */
    public function updateAction(Country $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Country $entity
     * @return array
     */
    protected function update(Country $entity)
    {
        return $this->get('oro_form.update_handler')->update(
            $entity,
            $this->get('oh.form.country'),
            'Success! Country created!',
            null,
            'oh_form_handler',
            null
        );
    }
}
