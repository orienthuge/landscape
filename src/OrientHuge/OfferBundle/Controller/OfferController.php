<?php

namespace OrientHuge\OfferBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class OfferController
 *
 * @package OrientHuge\OfferBundle\Controller
 */
class OfferController extends Controller
{
    /**
     * @Route("/", name="oh_offer_index")
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Offer::class];
    }

    /**
     * @Route("/view/{id}", name="oh_offer_view", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     * @param Offer $offer
     * @return array
     */
    public function viewAction(Offer $offer)
    {
        return ['entity' => $offer];
    }

    /**
     * @Route("/widget/info/{id}", name="oh_offer_widget_info", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeOfferBundle:Offer/widget:info.html.twig")
     * @param Offer $offer
     * @return array
     */
    public function infoAction(Offer $offer)
    {
        return ['offer' => $offer];
    }

    /**
     * @Route(
     *      "/widget/targets/{id}",
     *      name="oh_offer_widget_targets",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeOfferBundle:Offer/widget:targets.html.twig")
     * @param Offer|null $offer
     * @return array
     */
    public function targetsAction(Offer $offer = null)
    {
        return ['entity' => $offer];
    }
}
