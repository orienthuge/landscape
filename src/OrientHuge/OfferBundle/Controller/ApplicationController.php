<?php

namespace OrientHuge\OfferBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class ApplicationController
 *
 * @Route("/app")
 * @package OrientHuge\OfferBundle\Controller
 */
class ApplicationController extends Controller
{
    /**
     * @Route("/", name="oh_application_index")
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Application::class];
    }

    /**
     * @Route("/view/{id}", name="oh_application_view", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     * @param Application $application
     * @return array
     */
    public function viewAction(Application $application)
    {
        return ['entity' => $application];
    }

    /**
     * @Route(
     *      "/widget/offers/{id}",
     *      name="oh_application_widget_offers",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeOfferBundle:Application/widget:offers.html.twig")
     * @param Application|null $application
     * @return array
     */
    public function offersAction(Application $application = null)
    {
        return [
            'entity' => $application
        ];
    }

    /**
     * @Route("/widget/info/{id}", name="oh_application_widget_info", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeOfferBundle:Application/widget:info.html.twig")
     * @param Application $application
     * @return array
     */
    public function infoAction(Application $application)
    {
        return ['application' => $application];
    }
}
