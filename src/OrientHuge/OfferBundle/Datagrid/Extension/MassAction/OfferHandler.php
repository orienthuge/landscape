<?php

namespace OrientHuge\OfferBundle\Datagrid\Extension\MassAction;

use Doctrine\ORM\EntityManager;

use OrientHuge\CoreBundle\Constant\Status;
use OrientHuge\CoreBundle\Entity\Offer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;

use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionResponse;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerArgs;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerInterface;

class OfferHandler implements MassActionHandlerInterface
{
    const FLUSH_BATCH_SIZE = 100;

    /** @var AclHelper */
    protected $aclHelper;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var bool */
    protected $isEnabled;

    /** @var string */
    protected $successMessage;

    /** @var string */
    protected $errorMessage;

    /**
     * @param AclHelper             $aclHelper
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface   $translator
     * @param bool                  $isEnabled
     * @param string                $successMessage
     * @param string                $errorMessage
     */
    public function __construct(
        AclHelper $aclHelper,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        $isEnabled,
        $successMessage,
        $errorMessage
    ) {
        $this->aclHelper      = $aclHelper;
        $this->tokenStorage   = $tokenStorage;
        $this->translator     = $translator;
        $this->isEnabled      = $isEnabled;
        $this->successMessage = $successMessage;
        $this->errorMessage   = $errorMessage;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * {@inheritdoc}
     */
    public function handle(MassActionHandlerArgs $args)
    {
        $token = $this->tokenStorage->getToken();
        $count = 0;
        if ($token) {
            set_time_limit(0);
            $results = $args->getResults();
            $query   = $results->getSource();
            $this->aclHelper->apply($query, 'EDIT');
            $em = $results->getSource()->getEntityManager();

            $processedEntities = [];
            foreach ($results as $result) {
                if ($this->processOffer($result)) {
                    $count++;
                }
                $processedEntities[] = $result->getRootEntity();
                if ($count % self::FLUSH_BATCH_SIZE === 0) {
                    $this->finishBatch($em, $processedEntities);
                    $processedEntities = [];
                }
            }

            $this->finishBatch($em, $processedEntities);
        }

        return $count > 0
            ? new MassActionResponse(true, $this->translator->trans($this->successMessage, ['%count%' => $count]))
            : new MassActionResponse(false, $this->translator->trans($this->errorMessage, ['%count%' => $count]));
    }

    /**
     * @param ResultRecord $result
     *
     * @return bool
     */
    protected function processOffer(ResultRecord $result)
    {
        $offer = $result->getRootEntity();
        if (!$offer instanceof Offer) {
            return false;//unexpected result record
        }
        if ($offer->isEnabled() === $this->isEnabled) {
            return false;//do not count not affected records
        }
        $offer->setStatus($this->isEnabled ? Status::ENABLED : Status::PAUSED);

        return true;
    }

    /**
     * @param EntityManager $em
     * @param Offer[] $processedEntities
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    protected function finishBatch(EntityManager $em, $processedEntities)
    {
        foreach ($processedEntities as $entity) {
            $em->flush($entity);
            $em->detach($entity);
        }
    }
}
