<?php
/**
 * Created by PhpStorm.
 * User: rick
 * Date: 2018/5/17
 * Time: 18:15
 */
namespace OrientHuge\OfferBundle\Datagrid\Extension\MassAction;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionResponse;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerArgs;
use Oro\Bundle\DataGridBundle\Extension\MassAction\MassActionHandlerInterface;

class OfferPicker implements MassActionHandlerInterface
{
    /** @var AclHelper */
    protected $aclHelper;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var bool */
    protected $session;

    /** @var string */
    protected $successMessage;

    /** @var string */
    protected $errorMessage;

    /**
     * @param AclHelper             $aclHelper
     * @param TokenStorageInterface $tokenStorage
     * @param TranslatorInterface   $translator
     * @param SessionInterface      $session
     * @param string                $successMessage
     * @param string                $errorMessage
     */
    public function __construct(
        AclHelper $aclHelper,
        TokenStorageInterface $tokenStorage,
        TranslatorInterface $translator,
        SessionInterface $session,
        $successMessage,
        $errorMessage
    ) {
        $this->aclHelper      = $aclHelper;
        $this->tokenStorage   = $tokenStorage;
        $this->translator     = $translator;
        $this->session        = $session;
        $this->successMessage = $successMessage;
        $this->errorMessage   = $errorMessage;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(MassActionHandlerArgs $args)
    {
        $token = $this->tokenStorage->getToken();
        $count = 0;
        if ($token) {
            $results = $args->getResults();
            while($results->valid()) {
                $r = $results->current();
                $count ++;
                $results->next();
            }
            $pickedOffers = $this->session->get('picked_offers');
            $this->session->set('picked_offers', $pickedOffers);
        }

        return $count > 0
            ? new MassActionResponse(true, $this->translator->trans($this->successMessage, ['%count%' => $count]))
            : new MassActionResponse(false, $this->translator->trans($this->errorMessage, ['%count%' => $count]));
    }
}
