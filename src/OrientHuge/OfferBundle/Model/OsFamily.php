<?php

namespace OrientHuge\OfferBundle\Model;

class OsFamily
{
    const ANDROID = 'android';
    const IOS = 'ios';
    const WEB = 'web';
    const UNKNOWN = 'unknown';
}
