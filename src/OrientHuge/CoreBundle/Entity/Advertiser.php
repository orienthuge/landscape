<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\UserBundle\Entity\User;

/**
 * Advertiser
 *
 * @ORM\Table(name="oh_advertiser")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "ownership"={
 *              "owner_type"="BUSINESS_UNIT",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="business_unit_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"="",
 *              "category"="platform_management"
 *          },
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Advertiser extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, unique=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=1024)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $website = '';

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="text", nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $memo;

    /**
     * @var integer
     *
     * @ORM\Column(name="conversion_window", type="integer")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $conversionWindow = 28;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="assigned_to_user_id", referencedColumnName="id", onDelete="SET NULL")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          },
     *          "importexport"={
     *              "order"=200,
     *              "short"=true
     *          },
     *          "merge"={
     *              "display"=true
     *          }
     *      }
     * )
     */
    protected $assignedTo;

    /**
     * @var Account[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Account", mappedBy="advertiser", cascade={"persist"})
     */
    protected $accounts;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->accounts = new ArrayCollection();
    }

    /**
     * Set advertiser name
     *
     * @param string $name
     *
     * @return Advertiser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get advertiser name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return Advertiser
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * @return Account[]|Collection
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @param Account $account
     * @return Advertiser
     */
    public function addAccount(Account $account): Advertiser
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts->add($account);
            $account->setAdvertiser($this);
        }

        return $this;
    }

    /**
     * @param Account $account
     * @return Advertiser
     */
    public function removeAccount(Account $account): Advertiser
    {
        $this->accounts->removeElement($account);
        if ($account->getAdvertiser() === $this) {
            $account->setAdvertiser(null);
        }

        return $this;
    }

    /**
     * @param User $assignedTo
     * @return Advertiser
     */
    public function setAssignedTo(User $assignedTo): Advertiser
    {
        $this->assignedTo = $assignedTo;
        return $this;
    }

    /**
     * @return User
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @param string $website
     * @return Advertiser
     */
    public function setWebsite(string $website): Advertiser
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param int $conversionWindow
     * @return Advertiser
     */
    public function setConversionWindow(int $conversionWindow): Advertiser
    {
        $this->conversionWindow = $conversionWindow;
        return $this;
    }

    /**
     * @return int
     */
    public function getConversionWindow()
    {
        return $this->conversionWindow;
    }

    public function __toString()
    {
        return $this->name;
    }
}

