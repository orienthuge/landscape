<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;

/**
 * Country
 *
 * @ORM\Table(name="oh_offer_country")
 * @ORM\Entity()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=false},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_alpha2", type="string", length=2, unique=true)
     */
    private $codeAlpha2;

    /**
     * @var string
     *
     * @ORM\Column(name="code_alpha3", type="string", length=3, unique=true)
     */
    private $codeAlpha3;

    /**
     * @var string
     *
     * @ORM\Column(name="code_alias", type="string", length=3, nullable=true)
     */
    private $codeAlias;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=55)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeAlpha2
     *
     * @param string $codeAlpha2
     *
     * @return Country
     */
    public function setCodeAlpha2($codeAlpha2)
    {
        $this->codeAlpha2 = $codeAlpha2;

        return $this;
    }

    /**
     * Get codeAlpha2
     *
     * @return string
     */
    public function getCodeAlpha2()
    {
        return $this->codeAlpha2;
    }

    /**
     * Set codeAlpha3
     *
     * @param string $codeAlpha3
     *
     * @return Country
     */
    public function setCodeAlpha3($codeAlpha3)
    {
        $this->codeAlpha3 = $codeAlpha3;

        return $this;
    }

    /**
     * Get codeAlpha3
     *
     * @return string
     */
    public function getCodeAlpha3()
    {
        return $this->codeAlpha3;
    }

    /**
     * Set codeAlias
     *
     * @param string $codeAlias
     *
     * @return Country
     */
    public function setCodeAlias($codeAlias)
    {
        $this->codeAlias = $codeAlias;

        return $this;
    }

    /**
     * Get codeAlias
     *
     * @return string
     */
    public function getCodeAlias()
    {
        return $this->codeAlias;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

