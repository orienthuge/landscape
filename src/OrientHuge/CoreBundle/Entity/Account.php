<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;

use OrientHuge\CoreBundle\Constant\Status;

/**
 * Advertiser Account
 *
 * @ORM\Table(name="oh_advertiser_account")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity()
 * @Config(
 *      defaultValues={
 *          "ownership"={
 *              "owner_type"="BUSINESS_UNIT",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="business_unit_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"="",
 *              "category"="platform_management"
 *          },
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Account extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=45, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=145, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="request_url", type="string", length=1024, nullable=true)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $requestUrl;

    /**
     * @var bool
     *
     * @ORM\Column(name="hide_referrer", type="boolean")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $hideReferrer;

    /**
     * @var float
     *
     * @ORM\Column(name="payout_percent", type="decimal", precision=5, scale=4)
     * @ConfigField(
     *      defaultValues={
     *          "form"={
     *              "form_type"="oro_percent",
     *              "form_options"={
     *                  "constraints"={{"Range":{"min":80, "max":120}}},
     *              }
     *          },
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $payoutPercent = 1.1;

    /**
     * @var int
     *
     * @ORM\Column(name="daily_cap", type="integer")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $dailyCap = 200;

    /**
     * @var float
     *
     * @ORM\Column(name="daily_budget", type="decimal", precision=7, scale=2)
     * @ConfigField(
     *      defaultValues={
     *          "form"={
     *              "form_type"="oro_money",
     *              "form_options"={
     *                  "constraints"={{"Range":{"min":0}}},
     *              }
     *          },
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $dailyBudget = 2000;

    /**
     * @var integer
     *
     * @ORM\Column(name="fetch_interval", type="integer")
     */
    protected $fetchInterval = 30;

    /**
     * @var Advertiser
     *
     * @ORM\ManyToOne(targetEntity="Advertiser", inversedBy="accounts")
     * @ORM\JoinColumn(name="advertiser_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advertiser;

    /**
     * @var Offer[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="account", cascade={"all"}, orphanRemoval=true)
     */
    protected $offers;

    /**
     * @var ClickMacro[]|Collection
     *
     * @ORM\OneToMany(targetEntity="ClickMacro", mappedBy="account", cascade={"all"}, orphanRemoval=true)
     */
    protected $clickMacros;

    /**
     * @var ProfitModel[]|Collection
     *
     * @ORM\OneToMany(targetEntity="ProfitModel", mappedBy="account", cascade={"all"}, orphanRemoval=true)
     */
    protected $profitModels;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setStatus(Status::ENABLED);
        $this->offers = new ArrayCollection();
        $this->clickMacros = new ArrayCollection();
        $this->profitModels = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Account
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return Account
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Account
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set requestUrl
     *
     * @param string $requestUrl
     *
     * @return Account
     */
    public function setRequestUrl($requestUrl)
    {
        $this->requestUrl = $requestUrl;

        return $this;
    }

    /**
     * Get requestUrl
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * Set hideReferrer
     *
     * @param boolean $hideReferrer
     *
     * @return Account
     */
    public function setHideReferrer($hideReferrer)
    {
        $this->hideReferrer = $hideReferrer;

        return $this;
    }

    /**
     * Get hideReferrer
     *
     * @return bool
     */
    public function getHideReferrer()
    {
        return $this->hideReferrer;
    }

    /**
     * Set payoutPercent
     *
     * @param float $payoutPercent
     *
     * @return Account
     */
    public function setPayoutPercent($payoutPercent)
    {
        $this->payoutPercent = $payoutPercent;

        return $this;
    }

    /**
     * Get payoutPercent
     *
     * @return float
     */
    public function getPayoutPercent()
    {
        return $this->payoutPercent;
    }

    /**
     * Set dailyCap
     *
     * @param integer $dailyCap
     *
     * @return Account
     */
    public function setDailyCap($dailyCap)
    {
        $this->dailyCap = $dailyCap;

        return $this;
    }

    /**
     * Get dailyCap
     *
     * @return int
     */
    public function getDailyCap()
    {
        return $this->dailyCap;
    }

    /**
     * Set dailyBudget
     *
     * @param float $dailyBudget
     *
     * @return Account
     */
    public function setDailyBudget($dailyBudget)
    {
        $this->dailyBudget = $dailyBudget;

        return $this;
    }

    /**
     * Get dailyBudget
     *
     * @return float
     */
    public function getDailyBudget()
    {
        return $this->dailyBudget;
    }

    /**
     * Set advertiser
     *
     * @param Advertiser $advertiser
     *
     * @return Account
     */
    public function setAdvertiser(Advertiser $advertiser = null)
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * Get organization
     *
     * @return Advertiser
     */
    public function getAdvertiser()
    {
        return $this->advertiser;
    }

    /**
     * @return ClickMacro[]|Collection
     */
    public function getClickMacros()
    {
        return $this->clickMacros;
    }

    /**
     * Add specified click macro
     *
     * @param ClickMacro $macro
     *
     * @return Account
     */
    public function addClickMacro(ClickMacro $macro)
    {
        if (!$this->getClickMacros()->contains($macro)) {
            $this->getClickMacros()->add($macro);
            $macro->setAccount($this);
        }

        return $this;
    }

    /**
     * Remove specified macro
     *
     * @param ClickMacro $macro
     *
     * @return Account
     */
    public function removeClickMacro(ClickMacro $macro)
    {
        if ($this->getClickMacros()->contains($macro)) {
            $this->getClickMacros()->removeElement($macro);
        }

        return $this;
    }

    /**
     * @return Collection|ProfitModel[]
     */
    public function getProfitModels()
    {
        return $this->profitModels;
    }

    /**
     * Add specified profit model
     *
     * @param ProfitModel $model
     *
     * @return Account
     */
    public function addProfitModel(ProfitModel $model)
    {
        if (!$this->getProfitModels()->contains($model)) {
            $this->getProfitModels()->add($model);
            $model->setAccount($this);
        }

        return $this;
    }

    /**
     * Remove specified profit model
     *
     * @param ProfitModel $model
     *
     * @return Account
     */
    public function removeProfitModel(ProfitModel $model)
    {
        if ($this->getProfitModels()->contains($model)) {
            $this->getProfitModels()->removeElement($model);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return Account
     */
    public function addOffer(Offer $offer): Account
    {
        if (!$this->getOffers()->contains($offer)) {
            $this->getOffers()->add($offer);
            $offer->setAccount($this);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return Account
     */
    public function removeOffer(Offer $offer): Account
    {
        if ($this->getOffers()->contains($offer)) {
            $this->getOffers()->remove($offer);
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param int $interval
     * @return Account
     */
    public function setFetchInterval(int $interval): Account
    {
        $this->fetchInterval = $interval;
        return $this;
    }

    /**
     * @return int
     */
    public function getFetchInterval()
    {
        return $this->fetchInterval;
    }

    public function __toString()
    {
        return $this->name;
    }
}
