<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use OrientHuge\CoreBundle\Constant\Status;

/**
 * Blacklist
 *
 * @ORM\Table(name="oh_media_blacklist")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Blacklist extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="subaff_id", type="string", length=50)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $subAffId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="text", nullable=true)
     */
    private $reason;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="blacklists", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $media;


    /**
     * Account constructor.
     * @param Media|null $media
     */
    public function __construct(Media $media = null)
    {
        parent::__construct();

        if ($media) {
            $this->setMedia($media);
        }
        $this->setStatus(Status::ENABLED);
    }

    /**
     * @param $id
     * @return Blacklist
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set subAffId
     *
     * @param string $subAffId
     *
     * @return Blacklist
     */
    public function setSubAffId($subAffId)
    {
        $this->subAffId = $subAffId;

        return $this;
    }

    /**
     * Get subAffId
     *
     * @return string
     */
    public function getSubAffId()
    {
        return $this->subAffId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Blacklist
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return Blacklist
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media $media
     *
     * @return Blacklist
     */
    public function setMedia(Media $media): Blacklist
    {
        $this->media = $media;

        return $this;
    }
}

