<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use OrientHuge\CoreBundle\Model\ExtendConversion;

/**
 * Conversion
 *
 * @ORM\Table(name="oh_conversion")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Conversion extends ExtendConversion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tid", type="string", length=36)
     */
    protected $tid;

    /**
     * @var string
     *
     * @ORM\Column(name="aff_click_id", type="string", length=64)
     */
    protected $affClickId;

    /**
     * @var string
     *
     * @ORM\Column(name="conversion_ip", type="string", length=15)
     */
    protected $conversionIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="session_ip", type="string", length=15)
     */
    protected $sessionIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="postback_status", type="text", length=25)
     */
    protected $postbackStatus = '';

    /**
     * @var string
     *
     * @ORM\Column(name="postback_url", type="text", length=1024)
     */
    protected $postbackUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="postback_response", type="text", length=1024)
     */
    protected $postbackResponse = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="shaved", type="boolean")
     */
    protected $shaved = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="duplicated", type="boolean")
     */
    protected $duplicated = false;

    /**
     * @var float
     *
     * @ORM\Column(name="revenue", type="decimal", precision=7, scale=2)
     */
    protected $revenue = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="payout", type="decimal", precision=7, scale=2)
     */
    protected $payout = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    protected $media;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     */
    protected $offer;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get Conversion tid
     *
     * @return string
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getAffClickId(): string
    {
        return $this->affClickId;
    }

    /**
     * @return string
     */
    public function getConversionIp(): string
    {
        return $this->conversionIp;
    }

    /**
     * @return string
     */
    public function getSessionIp(): string
    {
        return $this->sessionIp;
    }

    /**
     * @return string
     */
    public function getPostbackStatus(): string
    {
        return $this->postbackStatus;
    }

    /**
     * @return string
     */
    public function getPostbackUrl(): string
    {
        return $this->postbackUrl;
    }

    /**
     * @return string
     */
    public function getPostbackResponse(): string
    {
        return $this->postbackResponse;
    }

    /**
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }

    /**
     * @return float
     */
    public function getPayout(): float
    {
        return $this->payout;
    }

    /**
     * @return bool
     */
    public function isDuplicated(): bool
    {
        return $this->duplicated;
    }

    /**
     * @return bool
     */
    public function isShaved(): bool
    {
        return $this->shaved;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }
}

