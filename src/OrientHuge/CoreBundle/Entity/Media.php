<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use OrientHuge\CoreBundle\Constant\Status;
use Hackzilla\PasswordGenerator\Generator\HybridPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;

/**
 * Media
 *
 * @ORM\Table(name="oh_media")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "ownership"={
 *              "owner_type"="BUSINESS_UNIT",
 *              "owner_field_name"="owner",
 *              "owner_column_name"="business_unit_owner_id",
 *              "organization_field_name"="organization",
 *              "organization_column_name"="organization_id"
 *          },
 *          "security"={
 *              "type"="ACL",
 *              "group_name"="",
 *              "category"="platform_management"
 *          },
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Media extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=6, nullable=true)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=20, nullable=true)
     */
    private $token;

    /**
     * @var int
     *
     * @ORM\Column(name="daily_cap", type="integer")
     */
    private $dailyCap = 2000;

    /**
     * @var float
     *
     * @ORM\Column(name="daily_credit", type="decimal", precision=7, scale=2)
     */
    private $dailyCredit = 20000;

    /**
     * @var int
     *
     * @ORM\Column(name="credit_level", type="integer")
     */
    private $creditLevel = 5;

    /**
     * @var ProfitModel[]|Collection
     *
     * @ORM\OneToMany(targetEntity="ProfitModel", mappedBy="media", cascade={"all"}, orphanRemoval=true)
     */
    protected $profitModels;

    /**
     * @var Blacklist[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Blacklist", mappedBy="media", cascade={"all"}, orphanRemoval=true)
     */
    protected $blacklists;

    /**
     * Media has Accounts
     * @var Collection|Account[]
     *
     * @ORM\ManyToMany(targetEntity="Account")
     * @ORM\JoinTable(name="oh_media_has_accounts",
     *      joinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $accounts;

    /**
     * @var Postback[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Postback", mappedBy="media", cascade={"all"}, orphanRemoval=true)
     */
    protected $postbacks;

    /**
     * Media has Offers
     * @var Collection|Offer[]
     *
     * @ORM\ManyToMany(targetEntity="Offer")
     * @ORM\JoinTable(name="oh_media_has_offers",
     *      joinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $offers;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setStatus(Status::ENABLED);
        $this->profitModels = new ArrayCollection();
        $this->blacklists = new ArrayCollection();
        $this->accounts = new ArrayCollection();
        $this->postbacks = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Media
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return Media
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Media
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set dailyCap
     *
     * @param integer $dailyCap
     *
     * @return Media
     */
    public function setDailyCap($dailyCap)
    {
        $this->dailyCap = $dailyCap;

        return $this;
    }

    /**
     * Get dailyCap
     *
     * @return int
     */
    public function getDailyCap()
    {
        return $this->dailyCap;
    }

    /**
     * Set dailyCredit
     *
     * @param float $dailyCredit
     *
     * @return Media
     */
    public function setDailyCredit($dailyCredit)
    {
        $this->dailyCredit = $dailyCredit;

        return $this;
    }

    /**
     * Get dailyCredit
     *
     * @return float
     */
    public function getDailyCredit()
    {
        return $this->dailyCredit;
    }

    /**
     * Set creditLevel
     *
     * @param integer $creditLevel
     *
     * @return Media
     */
    public function setCreditLevel($creditLevel)
    {
        $this->creditLevel = $creditLevel;

        return $this;
    }

    /**
     * Get creditLevel
     *
     * @return int
     */
    public function getCreditLevel()
    {
        return $this->creditLevel;
    }

    /**
     * @return Collection|ProfitModel[]
     */
    public function getProfitModels()
    {
        return $this->profitModels;
    }

    /**
     * Add specified profit model
     *
     * @param ProfitModel $model
     *
     * @return Media
     */
    public function addProfitModel(ProfitModel $model)
    {
        if (!$this->getProfitModels()->contains($model)) {
            $this->getProfitModels()->add($model);
            $model->setAccount($this->getAccounts()->current());
        }

        return $this;
    }

    /**
     * Remove specified profit model
     *
     * @param ProfitModel $model
     *
     * @return Media
     */
    public function removeProfitModel(ProfitModel $model)
    {
        if ($this->getProfitModels()->contains($model)) {
            $this->getProfitModels()->removeElement($model);
        }

        return $this;
    }

    /**
     * Add specified profit model
     *
     * @param Blacklist $blacklist
     *
     * @return Media
     */
    public function addBlacklist(Blacklist $blacklist)
    {
        if (!$this->getBlacklists()->contains($blacklist)) {
            $this->getBlacklists()->add($blacklist);
            $blacklist->setMedia($this);
        }

        return $this;
    }

    /**
     * Remove specified blacklist
     *
     * @param Blacklist $blacklist
     *
     * @return Media
     */
    public function removeBlacklist(Blacklist $blacklist)
    {
        if ($this->getBlacklists()->contains($blacklist)) {
            $this->getBlacklists()->removeElement($blacklist);
        }

        return $this;
    }

    /**
     * @return Collection|Blacklist[]
     */
    public function getBlacklists()
    {
        return $this->blacklists;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @param Account $account
     * @return Media
     */
    public function addAccount(Account $account): Media
    {
        if (!$this->getAccounts()->contains($account)) {
            $this->getAccounts()->add($account);
        }

        return $this;
    }

    /**
     * @param Account $account
     * @return Media
     */
    public function removeAccount(Account $account): Media
    {
        if ($this->getAccounts()->contains($account)) {
            $this->getAccounts()->remove($account);
        }

        return $this;
    }

    /**
     * Pre persist event listener
     *
     * @ORM\PrePersist
     */
    public function beforeSave()
    {
        parent::beforeSave();
        $this->setClientId($this->generateClientId());
        $this->setToken($this->generateToken());
    }

    /**
     * @return string
     */
    protected function generateClientId()
    {
        $generator =  new ComputerPasswordGenerator();
        return $generator
            ->setUppercase(false)
            ->setLowerCase(false)
            ->setNumbers()
            ->setSymbols(false)
            ->setLength(6)
            ->generatePassword()
            ;
    }

    /**
     * @return string
     */
    protected function generateToken()
    {
        $generator =  new HybridPasswordGenerator();
        return $generator
            ->setLength(19)
            ->setSegmentSeparator('-')
            ->setSymbols(false)
            ->setUppercase()
            ->setLowercase(false)
            ->setAvoidSimilar()
            ->setNumbers()
            ->setSegmentCount(4)
            ->setSegmentLength(4)
            ->generatePassword()
            ;
    }

    /**
     * @return Collection|Postback[]
     */
    public function getPostbacks()
    {
        return $this->postbacks;
    }

    /**
     * @param Postback $postback
     * @return Media
     */
    public function addPostback(Postback $postback): Media
    {
        if (!$this->getPostbacks()->contains($postback)) {
            $this->getPostbacks()->add($postback);
            $postback->setMedia($this);
        }

        return $this;
    }

    /**
     * @param Postback $postback
     * @return Media
     */
    public function removePostback(Postback $postback): Media
    {
        if ($this->getPostbacks()->contains($postback)) {
            $this->getPostbacks()->remove($postback);
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     * @return Media
     */
    public function addOffer(Offer $offer): Media
    {
        if (!$this->getOffers()->contains($offer)) {
            $this->getOffers()->add($offer);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return Media
     */
    public function removeOffer(Offer $offer): Media
    {
        if ($this->getOffers()->contains($offer)) {
            $this->getOffers()->remove($offer);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}

