<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use OrientHuge\CoreBundle\Constant\Status;

/**
 * Target
 *
 * @ORM\Table(name="oh_offer_target")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Target extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="country_lists", type="string", length=1024)
     */
    private $countrySet;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=5)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=4, scale=2)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="daily_budget", type="decimal", precision=7, scale=2)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $dailyBudget = 200.0;

    /**
     * @var int
     *
     * @ORM\Column(name="daily_cap", type="integer")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $dailyCap = 100;

    /**
     * @var Target[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Target", mappedBy="parent")
     */
    private $histories;

    /**
     * @var Target
     *
     * @ORM\ManyToOne(targetEntity="Target", inversedBy="histories")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=15, options={"default": "enabled"})
     */
    protected $status;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="targets")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     */
    private $offer;

    /**
     * Target constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setStatus(Status::ENABLED);
        $this->histories = new ArrayCollection();
    }

    /**
     * Set countries
     *
     * @param string|array $countries
     *
     * @return Target
     */
    public function setCountrySet($countries)
    {
        if (is_array($countries)) {
            $countries = join(",", $countries);
        }
        $this->countrySet = $countries;

        return $this;
    }

    /**
     * Get countries
     *
     * @return string
     */
    public function getCountrySet()
    {
        return $this->countrySet;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Target
     */
    public function setCurrency($currency): Target
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Target
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Target $target|null
     * @return $this
     */
    public function setParent(Target $target = null)
    {
        $this->parent = $target;

        return $this;
    }

    /**
     * @return Target
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set target
     *
     * @param Target $target
     *
     * @return Target
     */
    public function addHistory(Target $target): Target
    {
        if (!$this->getHistories()->contains($target)) {
            $this->getHistories()->add($target);
            $target->setParent($this);
        }

        return $this;
    }

    /**
     * @return Target[]|Collection
     */
    public function getHistories()
    {
        return $this->histories;
    }

    /**
     * @param Offer $offer
     * @return Target
     */
    public function setOffer(Offer $offer): Target
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param string $status
     * @return Target
     */
    public function setStatus(string $status): Target
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->getStatus() === Status::ENABLED;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return empty($this->getParent());
    }

    /**
     * @param float $dailyBudget
     * @return Target
     */
    public function setDailyBudget(float $dailyBudget): Target
    {
        $this->dailyBudget = $dailyBudget;
        return $this;
    }

    /**
     * @return float
     */
    public function getDailyBudget(): float
    {
        return $this->dailyBudget;
    }

    /**
     * @param int $dailyCap
     * @return Target
     */
    public function setDailyCap(int $dailyCap): Target
    {
        $this->dailyCap = $dailyCap;
        return $this;
    }

    /**
     * @return int
     */
    public function getDailyCap(): int
    {
        return $this->dailyCap;
    }
}

