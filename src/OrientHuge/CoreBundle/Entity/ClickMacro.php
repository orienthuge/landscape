<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\FormBundle\Entity\EmptyItem;

/**
 * ClickMacro
 *
 * @ORM\Table(name="oh_click_macro")
 * @ORM\Entity()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class ClickMacro implements EmptyItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=45)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=45)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_android", type="boolean")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $isAndroid = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_ios", type="boolean")
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    protected $isIos = true;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="clickMacros")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return ClickMacro
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return ClickMacro
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ClickMacro
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     *
     * @return ClickMacro
     */
    public function setAccount(Account $account = null): ClickMacro
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Is empty
     */
    public function isEmpty()
    {
        return empty($this->field) || empty($this->value);
    }

    /**
     * @param mixed $other
     * @return bool
     */
    public function isEqual($other)
    {
        $class = ClassUtils::getClass($this);

        /** @var ClickMacro $other */
        if (!$other instanceof $class) {
            return false;
        } elseif ($this->getId() && $other->getId()) {
            return $this->getId() == $other->getId();
        } elseif ($this->getId() || $other->getId()) {
            return false;
        } else {
            return $this == $other;
        }
    }

    /**
     * @param mixed $isAndroid
     * @return ClickMacro
     */
    public function setAndroid($isAndroid)
    {
        $this->isAndroid = $isAndroid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isAndroid()
    {
        return $this->isAndroid;
    }

    /**
     * @param bool $isIos
     * @return ClickMacro
     */
    public function setIos(bool $isIos): ClickMacro
    {
        $this->isIos = $isIos;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIos(): bool
    {
        return $this->isIos;
    }
}
