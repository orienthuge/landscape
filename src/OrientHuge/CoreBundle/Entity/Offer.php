<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\OrganizationBundle\Entity\BusinessUnit;
use Oro\Bundle\OrganizationBundle\Entity\Organization;
use OrientHuge\CoreBundle\Constant\Status;

/**
 * Offer
 *
 * @ORM\Table(name="oh_offer")
 * @ORM\Entity(repositoryClass="OrientHuge\CoreBundle\Entity\Repository\OfferRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Offer extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=15, options={"default": "enabled"})
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=15)
     */
    protected $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=15)
     */
    private $platform;

    /**
     * @var string
     *
     * @ORM\Column(name="click_url", type="string", length=1024)
     */
    private $clickUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="traffic_type", type="string", length=50)
     */
    private $trafficType;

    /**
     * @var string
     *
     * @ORM\Column(name="bid_type", type="string", length=15)
     */
    private $bidType;

    /**
     * @var string
     *
     * @ORM\Column(name="os_minversion", type="string", length=55, nullable=true)
     */
    private $osMinVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="os_maxversion", type="string", length=55, nullable=true)
     */
    private $osMaxVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="kpi", type="text", nullable=true)
     */
    private $kpi;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=45)
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="source_id", type="string", length=55, unique=true)
     */
    private $sourceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="launch_at", type="datetime", nullable=true)
     */
    private $launchAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     */
    private $expiredAt;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="offers", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;

    /**
     * @var Application
     *
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="offers", cascade={"persist"})
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $application;

    /**
     * @var Creative[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Creative", mappedBy="offer", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $creatives;

    /**
     * @var Target[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Target", mappedBy="offer", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"parent" = "ASC"})
     */
    protected $targets;

    /**
     * One Offer has One Target.
     * @var Target
     *
     * @ORM\OneToOne(targetEntity="Target")
     * @ORM\JoinColumn(name="target_id", referencedColumnName="id", nullable=true)
     */
    private $target;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $organization;

    /**
     * @var BusinessUnit
     * @ORM\ManyToOne(targetEntity="Oro\Bundle\OrganizationBundle\Entity\BusinessUnit")
     * @ORM\JoinColumn(name="business_unit_owner_id", referencedColumnName="id", onDelete="SET NULL")
     * @ConfigField(
     *      defaultValues={
     *          "importexport"={
     *              "excluded"=true
     *          }
     *      }
     * )
     */
    protected $owner;

    /**
     * Many Offers have Many Countries.
     * @var Collection|Country[]
     *
     * @ORM\ManyToMany(targetEntity="Country")
     * @ORM\JoinTable(name="oh_offer_has_countries",
     *      joinColumns={@ORM\JoinColumn(name="offer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id")}
     * )
     */
    private $countries;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->creatives = new ArrayCollection();
        $this->targets = new ArrayCollection();
        $this->countries = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Offer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Offer
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set clickUrl
     *
     * @param string $clickUrl
     *
     * @return Offer
     */
    public function setClickUrl($clickUrl)
    {
        $this->clickUrl = $clickUrl;

        return $this;
    }

    /**
     * Get clickUrl
     *
     * @return string
     */
    public function getClickUrl()
    {
        return $this->clickUrl;
    }

    /**
     * Set trafficType
     *
     * @param string $trafficType
     *
     * @return Offer
     */
    public function setTrafficType($trafficType)
    {
        $this->trafficType = $trafficType;

        return $this;
    }

    /**
     * Get trafficType
     *
     * @return string
     */
    public function getTrafficType()
    {
        return $this->trafficType;
    }

    /**
     * Set bidType
     *
     * @param string $bidType
     *
     * @return Offer
     */
    public function setBidType($bidType)
    {
        $this->bidType = $bidType;

        return $this;
    }

    /**
     * Get bidType
     *
     * @return string
     */
    public function getBidType()
    {
        return $this->bidType;
    }

    /**
     * Set osMinVersion
     *
     * @param string $osMinVersion
     *
     * @return Offer
     */
    public function setOsMinVersion($osMinVersion)
    {
        $this->osMinVersion = $osMinVersion;

        return $this;
    }

    /**
     * Get osMinVersion
     *
     * @return string
     */
    public function getOsMinVersion()
    {
        return $this->osMinVersion;
    }

    /**
     * Set osMaxVersion
     *
     * @param string $osMaxVersion
     *
     * @return Offer
     */
    public function setOsMaxVersion($osMaxVersion)
    {
        $this->osMaxVersion = $osMaxVersion;

        return $this;
    }

    /**
     * Get osMaxVersion
     *
     * @return string
     */
    public function getOsMaxVersion()
    {
        return $this->osMaxVersion;
    }

    /**
     * Set kpi
     *
     * @param string $kpi
     *
     * @return Offer
     */
    public function setKpi($kpi)
    {
        $this->kpi = $kpi;

        return $this;
    }

    /**
     * Get kpi
     *
     * @return string
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return Offer
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set sourceId
     *
     * @param string $sourceId
     *
     * @return Offer
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId
     *
     * @return string
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set launchAt
     *
     * @param \DateTime $launchAt
     *
     * @return Offer
     */
    public function setLaunchAt($launchAt)
    {
        $this->launchAt = $launchAt;

        return $this;
    }

    /**
     * Get launchAt
     *
     * @return \DateTime
     */
    public function getLaunchAt()
    {
        return $this->launchAt;
    }

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     *
     * @return Offer
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     *
     * @return Offer
     */
    public function setAccount(Account $account): Offer
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @param Application $application
     * @return Offer
     */
    public function setApplication(Application $application): Offer
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @return Application
     */
    public function getApplication(): Application
    {
        return $this->application;
    }

    /**
     * @return Collection|Creative[]
     */
    public function getCreatives()
    {
        return $this->creatives;
    }

    /**
     * @param Creative $creative
     * @return Offer
     */
    public function addCreative(Creative $creative): Offer
    {
        if(!$this->getCreatives()->contains($creative)) {
            $this->getCreatives()->add($creative);
            $creative->setOffer($this);
        }

        return $this;
    }

    /**
     * @param Creative $creative
     * @return Offer
     */
    public function removeCreative(Creative $creative): Offer
    {
        if($this->getCreatives()->contains($creative)) {
            $this->getCreatives()->remove($creative);
        }

        return $this;
    }

    /**
     * @param Target $target
     * @return Offer
     */
    public function addTarget(Target $target): Offer
    {
        if (!$this->getTargets()->contains($target)) {
            $this->getTargets()->add($target);
            $target->setOffer($this);
        }

        return $this;
    }

    /**
     * @param Target $target
     * @return Offer
     */
    public function removeTarget(Target $target): Offer
    {
        if ($this->getTargets()->contains($target)) {
            $this->getTargets()->remove($target);
        }

        return $this;
    }

    /**
     * @return Collection|Target[]
     */
    public function getTargets()
    {
        return $this->targets;
    }

    /**
     * @return Organization
     */
    public function getOrganization(): Organization
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;
    }

    /**
     * Get a business unit owning this report
     *
     * @return BusinessUnit
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set a business unit owning this report
     *
     * @param BusinessUnit $owningBusinessUnit
     * @return $this
     */
    public function setOwner(BusinessUnit $owningBusinessUnit)
    {
        $this->owner = $owningBusinessUnit;

        return $this;
    }

    /**
     * @param string $status
     * @return Offer
     */
    public function setStatus(string $status): Offer
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->getStatus() === Status::ENABLED;
    }

    /**
     * @param $id
     * @return Offer
     */
    public function setUuid($id)
    {
        $this->uuid = $id;
        return  $this;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param Country $country
     * @return Offer
     */
    public function addCountry(Country $country)
    {
        if (!$this->getCountries()->contains($country)) {
            $this->getCountries()->add($country);
        }

        return $this;
    }

    /**
     * @param Country $country
     * @return Offer
     */
    public function removeCountry(Country $country)
    {
        if ($this->getCountries()->contains($country)) {
            $this->getCountries()->remove($country);
        }

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @return Target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param Target $target
     * @return Offer
     */
    public function setTarget(Target $target): Offer
    {
        $this->target = $target;
        return $this;
    }
}
