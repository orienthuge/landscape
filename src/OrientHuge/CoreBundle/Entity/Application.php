<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;

/**
 * Application
 *
 * @ORM\Table(name="oh_application")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Application extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="os_family", type="string", length=15)
     */
    private $osFamily;

    /**
     * @var string
     *
     * @ORM\Column(name="pkg_name", type="string", length=255, unique=true)
     */
    private $pkgName;

    /**
     * @var string
     *
     * @ORM\Column(name="preview_url", type="string", length=1024, nullable=true)
     */
    private $previewUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_url", type="string", length=1024, nullable=true)
     */
    private $iconUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="min_install", type="string", length=25, nullable=true)
     */
    private $minInstall;

    /**
     * @var string
     *
     * @ORM\Column(name="max_install", type="string", length=25, nullable=true)
     */
    private $maxInstall;

    /**
     * @var string
     *
     * @ORM\Column(name="os_minversion", type="string", length=55)
     */
    private $osMinversion;

    /**
     * @var string
     *
     * @ORM\Column(name="os_maxversion", type="string", length=55, nullable=true)
     */
    private $osMaxversion;

    /**
     * @var string
     *
     * @ORM\Column(name="pkg_size", type="string", length=20, nullable=true)
     */
    private $pkgSize;

    /**
     * @var Offer[]|Collection
     *
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $offers;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Application
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set osFamily
     *
     * @param string $osFamily
     *
     * @return Application
     */
    public function setOsFamily($osFamily)
    {
        $this->osFamily = $osFamily;

        return $this;
    }

    /**
     * Get osFamily
     *
     * @return string
     */
    public function getOsFamily()
    {
        return $this->osFamily;
    }

    /**
     * Set pkgName
     *
     * @param string $pkgName
     *
     * @return Application
     */
    public function setPkgName($pkgName)
    {
        $this->pkgName = $pkgName;

        return $this;
    }

    /**
     * Get pkgName
     *
     * @return string
     */
    public function getPkgName()
    {
        return $this->pkgName;
    }

    /**
     * Set previewUrl
     *
     * @param string $previewUrl
     *
     * @return Application
     */
    public function setPreviewUrl($previewUrl)
    {
        $this->previewUrl = $previewUrl;

        return $this;
    }

    /**
     * Get previewUrl
     *
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return Application
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Application
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set minInstall
     *
     * @param string $minInstall
     *
     * @return Application
     */
    public function setMinInstall($minInstall)
    {
        $this->minInstall = $minInstall;

        return $this;
    }

    /**
     * Get minInstall
     *
     * @return string
     */
    public function getMinInstall()
    {
        return $this->minInstall;
    }

    /**
     * Set maxInstall
     *
     * @param string $maxInstall
     *
     * @return Application
     */
    public function setMaxInstall($maxInstall)
    {
        $this->maxInstall = $maxInstall;

        return $this;
    }

    /**
     * Get maxInstall
     *
     * @return string
     */
    public function getMaxInstall()
    {
        return $this->maxInstall;
    }

    /**
     * Set osMinversion
     *
     * @param string $osMinversion
     *
     * @return Application
     */
    public function setOsMinversion($osMinversion)
    {
        $this->osMinversion = $osMinversion;

        return $this;
    }

    /**
     * Get osMinversion
     *
     * @return string
     */
    public function getOsMinversion()
    {
        return $this->osMinversion;
    }

    /**
     * Set osMaxversion
     *
     * @param string $osMaxversion
     *
     * @return Application
     */
    public function setOsMaxversion($osMaxversion)
    {
        $this->osMaxversion = $osMaxversion;

        return $this;
    }

    /**
     * Get osMaxversion
     *
     * @return string
     */
    public function getOsMaxversion()
    {
        return $this->osMaxversion;
    }

    /**
     * Set pkgSize
     *
     * @param string $pkgSize
     *
     * @return Application
     */
    public function setPkgSize($pkgSize)
    {
        $this->pkgSize = $pkgSize;

        return $this;
    }

    /**
     * Get pkgSize
     *
     * @return string
     */
    public function getPkgSize()
    {
        return $this->pkgSize;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     * @return Application
     */
    public function addOffer(Offer $offer): Application
    {
        if (!$this->getOffers()->contains($offer)) {
            $this->getOffers()->add($offer);
            $offer->setApplication($this);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return Application
     */
    public function removeOffer(Offer $offer): Application
    {
        if ($this->getOffers()->contains($offer)) {
            $this->getOffers()->remove($offer);
        }

        return $this;
    }
}

