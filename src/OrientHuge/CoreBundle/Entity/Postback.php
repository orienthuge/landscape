<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;
use Oro\Bundle\FormBundle\Entity\EmptyItem;

/**
 * Postback
 *
 * @ORM\Table(name="oh_media_postback")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=true},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class Postback extends BaseEntity implements EmptyItem
{
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1024)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $url;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="postbacks", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $media;

    /**
     * Account constructor.
     * @param Media|null $media
     */
    public function __construct(Media $media = null)
    {
        parent::__construct();

        if ($media) {
            $this->setMedia($media);
        }
    }

    /**
     * @param $id
     * @return Postback
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Postback
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media $media
     *
     * @return Postback
     */
    public function setMedia(Media $media): Postback
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Is empty
     */
    public function isEmpty()
    {
        return empty($this->url);
    }

    /**
     * @param mixed $other
     * @return bool
     */
    public function isEqual($other)
    {
        $class = ClassUtils::getClass($this);

        /** @var ClickMacro $other */
        if (!$other instanceof $class) {
            return false;
        } elseif ($this->getId() && $other->getId()) {
            return $this->getId() == $other->getId();
        } elseif ($this->getId() || $other->getId()) {
            return false;
        } else {
            return $this == $other;
        }
    }
}

