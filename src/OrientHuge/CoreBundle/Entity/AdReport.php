<?php

namespace OrientHuge\CoreBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;

/**
 * AdReport
 *
 * @ORM\Table(name="oh_ad_report")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=false},
 *          "tag"={"enabled"=false},
 *          "merge"={"enable"=false}
 *      }
 * )
 */
class AdReport extends BaseEntity
{
    /**
     * @var DateTime date
     *
     * @ORM\Column(name="report_date", type="datetime")
     */
    protected $date;

    /**
     * @var Advertiser
     *
     * @ORM\ManyToOne(targetEntity="Advertiser")
     * @ORM\JoinColumn(name="advertiser_id", referencedColumnName="id")
     */
    protected $advertiser;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    protected $media;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="Offer")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     */
    protected $offer;

    /**
     * @var integer
     *
     * @ORM\Column(name="impression", type="integer")
     */
    protected $impression;

    /**
     * @var integer
     *
     * @ORM\Column(name="click", type="integer")
     */
    protected $click;

    /**
     * @var integer
     *
     * @ORM\Column(name="conversion", type="integer")
     */
    protected $conversion;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="money")
     */
    protected $price;

    /**
     * @var float
     *
     * @ORM\Column(name="cpm", type="money")
     */
    protected $cpm;

    /**
     * @var float
     *
     * @ORM\Column(name="revenue", type="money")
     */
    protected $revenue;

    /**
     * Report constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return Advertiser
     */
    public function getAdvertiser(): Advertiser
    {
        return $this->advertiser;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @return Media
     */
    public function getMedia(): Media
    {
        return $this->media;
    }

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @return int
     */
    public function getImpression(): int
    {
        return $this->impression;
    }

    /**
     * @return int
     */
    public function getClick(): int
    {
        return $this->click;
    }

    /**
     * @return int
     */
    public function getConversion(): int
    {
        return $this->conversion;
    }

    /**
     * @param int $impression
     * @return AdReport
     */
    public function setImpression(int $impression): AdReport
    {
        $this->impression = $impression;
        return $this;
    }

    /**
     * @param int $click
     * @return AdReport
     */
    public function setClick(int $click): AdReport
    {
        $this->click = $click;
        return $this;
    }

    /**
     * @param int $conversion
     * @return AdReport
     */
    public function setConversion(int $conversion): AdReport
    {
        $this->conversion = $conversion;
        return $this;
    }

    /**
     * @param Advertiser $advertiser
     * @return AdReport
     */
    public function setAdvertiser(Advertiser $advertiser): AdReport
    {
        $this->advertiser = $advertiser;
        return $this;
    }

    /**
     * @param Account $account
     * @return AdReport
     */
    public function setAccount(Account $account): AdReport
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @param Media $media
     * @return AdReport
     */
    public function setMedia(Media $media): AdReport
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param Offer $offer
     * @return AdReport
     */
    public function setOffer(Offer $offer): AdReport
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @param DateTime $date
     * @return AdReport
     */
    public function setDate(DateTime $date): AdReport
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param float $price
     * @return AdReport
     */
    public function setPrice(float $price): AdReport
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $revenue
     * @return AdReport
     */
    public function setRevenue(float $revenue): AdReport
    {
        $this->revenue = $revenue;
        return $this;
    }

    /**
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }

    /**
     * @param float $cpm
     * @return AdReport
     */
    public function setCpm(float $cpm): AdReport
    {
        $this->cpm = $cpm;
        return $this;
    }

    /**
     * @return float
     */
    public function getCpm(): float
    {
        return $this->cpm;
    }
}
