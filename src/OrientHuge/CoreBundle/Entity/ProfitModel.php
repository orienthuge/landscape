<?php

namespace OrientHuge\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\Config;
use Oro\Bundle\EntityConfigBundle\Metadata\Annotation\ConfigField;

/**
 * ProfitModel
 *
 * @ORM\Table(name="oh_profit_model")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Config(
 *      defaultValues={
 *          "dataaudit"={"auditable"=true},
 *          "tag"={"enabled"=false},
 *          "merge"={"enable"=true}
 *      }
 * )
 */
class ProfitModel extends BaseEntity
{
    /**
     * @var float
     *
     * @ORM\Column(name="payout_percent", type="decimal", precision=5, scale=4)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $payoutPercent = 1.1;

    /**
     * @var float
     *
     * @ORM\Column(name="shave_rate", type="decimal", precision=5, scale=4)
     * @ConfigField(
     *      defaultValues={
     *          "dataaudit"={
     *              "auditable"=true
     *          }
     *      }
     * )
     */
    private $shaveRate = 0.15;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="text", nullable=true)
     */
    private $memo;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="profitModels", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="profitModels", cascade={"persist"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $media;


    /**
     * Set payoutPercent
     * datagrid online edit 的 bug，如果 front_end type 设置为 percent,
     * 修改时则会直接显示乘以100后的值，容易触发误操作，导致401 Unauthorized
     * 需要重新登录认证。
     *
     * @param float $payoutPercent
     *
     * @return ProfitModel
     */
    public function setPayoutPercent($payoutPercent)
    {
        $this->payoutPercent = $payoutPercent;
        if ($this->payoutPercent > 2) {
            $this->payoutPercent /= 100;
        }

        return $this;
    }

    /**
     * Get payoutPercent
     *
     * @return float
     */
    public function getPayoutPercent()
    {
        return $this->payoutPercent;
    }

    /**
     * Set shaveRate
     * datagrid online edit 的 bug，如果 front_end type 设置为 percent,
     * 修改时则会直接显示乘以100后的值，容易触发误操作，导致401 Unauthorized
     * 需要重新登录认证。
     *
     * @param float $shaveRate
     *
     * @return ProfitModel
     */
    public function setShaveRate($shaveRate)
    {
        $this->shaveRate = $shaveRate;
        if ($this->shaveRate >= 2) {
            $this->shaveRate /= 100;
        }

        return $this;
    }

    /**
     * Get shaveRate
     *
     * @return float
     */
    public function getShaveRate()
    {
        return $this->shaveRate;
    }

    /**
     * Set memo
     *
     * @param string $memo
     *
     * @return ProfitModel
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     *
     * @return ProfitModel
     */
    public function setAccount(Account $account = null): ProfitModel
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media $media
     * @return ProfitModel
     */
    public function setMedia(Media $media): ProfitModel
    {
        $this->media = $media;

        return $this;
    }
}

