<?php
namespace OrientHuge\CoreBundle\Migrations\Schema\v1_1;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class AdReport implements Migration
{
    /**
     * {@inheritdoc}
     * @throws SchemaException
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $this->createOhAdReportTable($schema);
        $this->addOhAdReportForeignKeys($schema);
    }

    /**
     * Create oh_ad_report table
     *
     * @param Schema $schema
     */
    protected function createOhAdReportTable(Schema $schema)
    {
        $table = $schema->createTable('oh_ad_report');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('advertiser_id', 'integer', ['notnull' => false]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('offer_id', 'integer', ['notnull' => false]);
        $table->addColumn('impression', 'integer', []);
        $table->addColumn('click', 'integer', []);
        $table->addColumn('conversion', 'integer', []);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['advertiser_id'], 'IDX_3CD2EB71BA2FCBC2', []);
        $table->addIndex(['account_id'], 'IDX_3CD2EB719B6B5FBA', []);
        $table->addIndex(['media_id'], 'IDX_3CD2EB71EA9FDD75', []);
        $table->addIndex(['offer_id'], 'IDX_3CD2EB7153C674EE', []);
    }

    /**
     * Add oh_ad_report foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhAdReportForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_ad_report');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser'),
            ['advertiser_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
    }
}
