<?php
namespace OrientHuge\CoreBundle\Migrations\Schema\v1_3;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class ReportDate implements Migration
{
    /**
     * {@inheritdoc}
     * @throws SchemaException
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('oh_ad_report');
        if ($table) {
            $table->addColumn('report_date', 'datetime', []);
            $table->addColumn('price', 'decimal', ['precision' => 7, 'scale' => 2]);
            $table->addColumn('cpm', 'decimal', ['precision' => 7, 'scale' => 2]);
            $table->addColumn('revenue', 'decimal', ['precision' => 10, 'scale' => 2]);
        }
    }
}
