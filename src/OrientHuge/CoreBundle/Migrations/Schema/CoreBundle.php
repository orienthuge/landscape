<?php

namespace OrientHuge\CoreBundle\Migrations\Schema;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\MigrationBundle\Migration\Installation;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class CoreBundle implements Installation
{
    /**
     * {@inheritdoc}
     */
    public function getMigrationVersion()
    {
        return 'v1_0';
    }

    /**
     * {@inheritdoc}
     * @throws SchemaException
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /** Tables generation **/
        $this->createOhAdvertiserTable($schema);
        $this->createOhAdvertiserAccountTable($schema);
        $this->createOhClickMacroTable($schema);
        $this->createOhConversionTable($schema);
        $this->createOhMediaTable($schema);
        $this->createOhMediaBlacklistTable($schema);
        $this->createOhMediaHasAccountsTable($schema);
        $this->createOhMediaHasOffersTable($schema);
        $this->createOhMediaPostbackTable($schema);
        $this->createOhOfferTable($schema);
        $this->createOhOfferCountryTable($schema);
        $this->createOhOfferCreativeTable($schema);
        $this->createOhOfferHasCountriesTable($schema);
        $this->createOhOfferTargetTable($schema);
        $this->createOhProfitModelTable($schema);
        $this->createOhReportTable($schema);
        $this->createOhApplicationTable($schema);

        /** Foreign keys generation **/
        $this->addOhAdvertiserForeignKeys($schema);
        $this->addOhAdvertiserAccountForeignKeys($schema);
        $this->addOhClickMacroForeignKeys($schema);
        $this->addOhConversionForeignKeys($schema);
        $this->addOhMediaForeignKeys($schema);
        $this->addOhMediaBlacklistForeignKeys($schema);
        $this->addOhMediaHasAccountsForeignKeys($schema);
        $this->addOhMediaHasOffersForeignKeys($schema);
        $this->addOhMediaPostbackForeignKeys($schema);
        $this->addOhOfferForeignKeys($schema);
        $this->addOhOfferCreativeForeignKeys($schema);
        $this->addOhOfferHasCountriesForeignKeys($schema);
        $this->addOhOfferTargetForeignKeys($schema);
        $this->addOhProfitModelForeignKeys($schema);
        $this->addOhReportForeignKeys($schema);
    }

    /**
     * Create oh_advertiser table
     *
     * @param Schema $schema
     */
    protected function createOhAdvertiserTable(Schema $schema)
    {
        $table = $schema->createTable('oh_advertiser');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('assigned_to_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('business_unit_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('created_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('updated_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('name', 'string', ['length' => 45]);
        $table->addColumn('website', 'string', ['length' => 1024]);
        $table->addColumn('memo', 'text', ['notnull' => false, 'length' => 0]);
        $table->addColumn('conversion_window', 'integer', []);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name'], 'UNIQ_935435F95E237E06');
        $table->addIndex(['assigned_to_user_id'], 'IDX_935435F911578D11', []);
        $table->addIndex(['organization_id'], 'IDX_935435F932C8A3DE', []);
        $table->addIndex(['business_unit_owner_id'], 'IDX_935435F959294170', []);
        $table->addIndex(['created_by_user_id'], 'IDX_935435F97D182D95', []);
        $table->addIndex(['updated_by_user_id'], 'IDX_935435F92793CC5E', []);
    }

    /**
     * Create oh_advertiser_account table
     *
     * @param Schema $schema
     */
    protected function createOhAdvertiserAccountTable(Schema $schema)
    {
        $table = $schema->createTable('oh_advertiser_account');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('advertiser_id', 'integer', ['notnull' => false]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('business_unit_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('created_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('updated_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('name', 'string', ['length' => 45]);
        $table->addColumn('status', 'string', ['length' => 10]);
        $table->addColumn('client_id', 'string', ['notnull' => false, 'length' => 45]);
        $table->addColumn('token', 'string', ['notnull' => false, 'length' => 145]);
        $table->addColumn('request_url', 'string', ['notnull' => false, 'length' => 1024]);
        $table->addColumn('hide_referrer', 'boolean', []);
        $table->addColumn('payout_percent', 'decimal', ['precision' => 5, 'scale' => 4]);
        $table->addColumn('daily_cap', 'integer', []);
        $table->addColumn('daily_budget', 'decimal', ['precision' => 7, 'scale' => 2]);
        $table->addColumn('fetch_interval', 'integer', []);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name'], 'UNIQ_12B8256C5E237E06');
        $table->addIndex(['advertiser_id'], 'IDX_12B8256CBA2FCBC2', []);
        $table->addIndex(['organization_id'], 'IDX_12B8256C32C8A3DE', []);
        $table->addIndex(['business_unit_owner_id'], 'IDX_12B8256C59294170', []);
        $table->addIndex(['created_by_user_id'], 'IDX_12B8256C7D182D95', []);
        $table->addIndex(['updated_by_user_id'], 'IDX_12B8256C2793CC5E', []);
    }

    /**
     * Create oh_click_macro table
     *
     * @param Schema $schema
     */
    protected function createOhClickMacroTable(Schema $schema)
    {
        $table = $schema->createTable('oh_click_macro');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('field', 'string', ['length' => 45]);
        $table->addColumn('value', 'string', ['length' => 45]);
        $table->addColumn('is_android', 'boolean', []);
        $table->addColumn('is_ios', 'boolean', []);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['account_id'], 'IDX_A16099559B6B5FBA', []);
    }

    /**
     * Create oh_conversion table
     *
     * @param Schema $schema
     */
    protected function createOhConversionTable(Schema $schema)
    {
        $table = $schema->createTable('oh_conversion');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('offer_id', 'integer', ['notnull' => false]);
        $table->addColumn('tid', 'string', ['length' => 36]);
        $table->addColumn('aff_click_id', 'string', ['length' => 64]);
        $table->addColumn('conversion_ip', 'string', ['length' => 15]);
        $table->addColumn('session_ip', 'string', ['length' => 15]);
        $table->addColumn('postback_status', 'text', ['length' => 255]);
        $table->addColumn('postback_url', 'text', ['length' => 65535]);
        $table->addColumn('postback_response', 'text', ['length' => 65535]);
        $table->addColumn('shaved', 'boolean', []);
        $table->addColumn('duplicated', 'boolean', []);
        $table->addColumn('revenue', 'decimal', ['precision' => 7, 'scale' => 2]);
        $table->addColumn('payout', 'decimal', ['precision' => 7, 'scale' => 2]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['account_id'], 'IDX_BDF435CF9B6B5FBA', []);
        $table->addIndex(['media_id'], 'IDX_BDF435CFEA9FDD75', []);
        $table->addIndex(['offer_id'], 'IDX_BDF435CF53C674EE', []);
    }

    /**
     * Create oh_media table
     *
     * @param Schema $schema
     */
    protected function createOhMediaTable(Schema $schema)
    {
        $table = $schema->createTable('oh_media');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('business_unit_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('created_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('updated_by_user_id', 'integer', ['notnull' => false]);
        $table->addColumn('name', 'string', ['length' => 45]);
        $table->addColumn('status', 'string', ['length' => 10]);
        $table->addColumn('client_id', 'string', ['notnull' => false, 'length' => 6]);
        $table->addColumn('token', 'string', ['notnull' => false, 'length' => 20]);
        $table->addColumn('daily_cap', 'integer', []);
        $table->addColumn('daily_credit', 'decimal', ['precision' => 7, 'scale' => 2]);
        $table->addColumn('credit_level', 'integer', []);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name'], 'UNIQ_856683CF5E237E06');
        $table->addIndex(['organization_id'], 'IDX_856683CF32C8A3DE', []);
        $table->addIndex(['business_unit_owner_id'], 'IDX_856683CF59294170', []);
        $table->addIndex(['created_by_user_id'], 'IDX_856683CF7D182D95', []);
        $table->addIndex(['updated_by_user_id'], 'IDX_856683CF2793CC5E', []);
    }

    /**
     * Create oh_media_blacklist table
     *
     * @param Schema $schema
     */
    protected function createOhMediaBlacklistTable(Schema $schema)
    {
        $table = $schema->createTable('oh_media_blacklist');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('subaff_id', 'string', ['length' => 50]);
        $table->addColumn('status', 'string', ['length' => 10]);
        $table->addColumn('reason', 'text', ['notnull' => false, 'length' => 0]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['media_id'], 'IDX_6C4DF757EA9FDD75', []);
    }

    /**
     * Create oh_media_has_accounts table
     *
     * @param Schema $schema
     */
    protected function createOhMediaHasAccountsTable(Schema $schema)
    {
        $table = $schema->createTable('oh_media_has_accounts');
        $table->addColumn('media_id', 'integer', []);
        $table->addColumn('account_id', 'integer', []);
        $table->setPrimaryKey(['media_id', 'account_id']);
        $table->addIndex(['media_id'], 'IDX_14764496EA9FDD75', []);
        $table->addIndex(['account_id'], 'IDX_147644969B6B5FBA', []);
    }

    /**
     * Create oh_media_has_offers table
     *
     * @param Schema $schema
     */
    protected function createOhMediaHasOffersTable(Schema $schema)
    {
        $table = $schema->createTable('oh_media_has_offers');
        $table->addColumn('media_id', 'integer', []);
        $table->addColumn('offer_id', 'integer', []);
        $table->setPrimaryKey(['media_id', 'offer_id']);
        $table->addIndex(['media_id'], 'IDX_9EEEFAE0EA9FDD75', []);
        $table->addIndex(['offer_id'], 'IDX_9EEEFAE053C674EE', []);
    }

    /**
     * Create oh_media_postback table
     *
     * @param Schema $schema
     */
    protected function createOhMediaPostbackTable(Schema $schema)
    {
        $table = $schema->createTable('oh_media_postback');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('url', 'string', ['length' => 1024]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['media_id'], 'IDX_4BF3A2CAEA9FDD75', []);
    }

    /**
     * Create oh_offer table
     *
     * @param Schema $schema
     */
    protected function createOhOfferTable(Schema $schema)
    {
        $table = $schema->createTable('oh_offer');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('application_id', 'integer', ['notnull' => false]);
        $table->addColumn('target_id', 'integer', ['notnull' => false]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('business_unit_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('name', 'string', ['length' => 255]);
        $table->addColumn('status', 'string', ['default' => 'enabled', 'length' => 15]);
        $table->addColumn('uuid', 'string', ['length' => 15]);
        $table->addColumn('platform', 'string', ['length' => 15]);
        $table->addColumn('click_url', 'string', ['length' => 1024]);
        $table->addColumn('traffic_type', 'string', ['length' => 50]);
        $table->addColumn('bid_type', 'string', ['length' => 15]);
        $table->addColumn('os_minversion', 'string', ['notnull' => false, 'length' => 55]);
        $table->addColumn('os_maxversion', 'string', ['notnull' => false, 'length' => 55]);
        $table->addColumn('kpi', 'text', ['notnull' => false, 'length' => 0]);
        $table->addColumn('external_id', 'string', ['length' => 45]);
        $table->addColumn('source_id', 'string', ['length' => 55]);
        $table->addColumn('launch_at', 'datetime', ['notnull' => false, 'length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('expired_at', 'datetime', ['notnull' => false, 'length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['source_id'], 'UNIQ_C69CA5FD953C1C61');
        $table->addUniqueIndex(['target_id'], 'UNIQ_C69CA5FD158E0B66');
        $table->addIndex(['account_id'], 'IDX_C69CA5FD9B6B5FBA', []);
        $table->addIndex(['application_id'], 'IDX_C69CA5FD3E030ACD', []);
        $table->addIndex(['organization_id'], 'IDX_C69CA5FD32C8A3DE', []);
        $table->addIndex(['business_unit_owner_id'], 'IDX_C69CA5FD59294170', []);
    }

    /**
     * Create oh_offer_country table
     *
     * @param Schema $schema
     */
    protected function createOhOfferCountryTable(Schema $schema)
    {
        $table = $schema->createTable('oh_offer_country');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('code_alpha2', 'string', ['length' => 2]);
        $table->addColumn('code_alpha3', 'string', ['length' => 3]);
        $table->addColumn('code_alias', 'string', ['notnull' => false, 'length' => 3]);
        $table->addColumn('name', 'string', ['length' => 55]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['code_alpha2'], 'UNIQ_5A371E0AC8E48ED0');
        $table->addUniqueIndex(['code_alpha3'], 'UNIQ_5A371E0ABFE3BE46');
    }

    /**
     * Create oh_offer_creative table
     *
     * @param Schema $schema
     */
    protected function createOhOfferCreativeTable(Schema $schema)
    {
        $table = $schema->createTable('oh_offer_creative');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('offer_id', 'integer', ['notnull' => false]);
        $table->addColumn('name', 'string', ['length' => 45]);
        $table->addColumn('type', 'string', ['length' => 15]);
        $table->addColumn('width', 'integer', ['notnull' => false]);
        $table->addColumn('height', 'integer', ['notnull' => false]);
        $table->addColumn('ratio', 'string', ['notnull' => false, 'length' => 15]);
        $table->addColumn('size', 'string', ['notnull' => false, 'length' => 10]);
        $table->addColumn('duration', 'integer', ['notnull' => false]);
        $table->addColumn('url', 'string', ['length' => 1024]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['offer_id'], 'IDX_1F319C4053C674EE', []);
    }

    /**
     * Create oh_offer_has_countries table
     *
     * @param Schema $schema
     */
    protected function createOhOfferHasCountriesTable(Schema $schema)
    {
        $table = $schema->createTable('oh_offer_has_countries');
        $table->addColumn('offer_id', 'integer', []);
        $table->addColumn('country_id', 'integer', []);
        $table->setPrimaryKey(['offer_id', 'country_id']);
        $table->addIndex(['offer_id'], 'IDX_2387E16F53C674EE', []);
        $table->addIndex(['country_id'], 'IDX_2387E16FF92F3E70', []);
    }

    /**
     * Create oh_offer_target table
     *
     * @param Schema $schema
     */
    protected function createOhOfferTargetTable(Schema $schema)
    {
        $table = $schema->createTable('oh_offer_target');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('parent_id', 'integer', ['notnull' => false]);
        $table->addColumn('offer_id', 'integer', ['notnull' => false]);
        $table->addColumn('country_lists', 'string', ['length' => 1024]);
        $table->addColumn('currency', 'string', ['length' => 5]);
        $table->addColumn('price', 'decimal', ['precision' => 4, 'scale' => 2]);
        $table->addColumn('daily_budget', 'decimal', ['precision' => 7, 'scale' => 2]);
        $table->addColumn('daily_cap', 'integer', []);
        $table->addColumn('status', 'string', ['default' => 'enabled', 'length' => 15]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['parent_id'], 'IDX_B4F468F0727ACA70', []);
        $table->addIndex(['offer_id'], 'IDX_B4F468F053C674EE', []);
    }

    /**
     * Create oh_profit_model table
     *
     * @param Schema $schema
     */
    protected function createOhProfitModelTable(Schema $schema)
    {
        $table = $schema->createTable('oh_profit_model');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('payout_percent', 'decimal', ['precision' => 5, 'scale' => 4]);
        $table->addColumn('shave_rate', 'decimal', ['precision' => 5, 'scale' => 4]);
        $table->addColumn('memo', 'text', ['notnull' => false, 'length' => 0]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['account_id'], 'IDX_E6664B999B6B5FBA', []);
        $table->addIndex(['media_id'], 'IDX_E6664B99EA9FDD75', []);
    }

    /**
     * Create oh_report table
     *
     * @param Schema $schema
     */
    protected function createOhReportTable(Schema $schema)
    {
        $table = $schema->createTable('oh_report');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('advertiser_id', 'integer', ['notnull' => false]);
        $table->addColumn('account_id', 'integer', ['notnull' => false]);
        $table->addColumn('media_id', 'integer', ['notnull' => false]);
        $table->addColumn('offer_id', 'integer', ['notnull' => false]);
        $table->addColumn('impression', 'integer', []);
        $table->addColumn('click', 'integer', []);
        $table->addColumn('conversion', 'integer', []);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['advertiser_id'], 'IDX_C6ADAEACBA2FCBC2', []);
        $table->addIndex(['account_id'], 'IDX_C6ADAEAC9B6B5FBA', []);
        $table->addIndex(['media_id'], 'IDX_C6ADAEACEA9FDD75', []);
        $table->addIndex(['offer_id'], 'IDX_C6ADAEAC53C674EE', []);
    }

    /**
     * Create oh_application table
     *
     * @param Schema $schema
     */
    protected function createOhApplicationTable(Schema $schema)
    {
        $table = $schema->createTable('oh_application');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('name', 'string', ['length' => 255]);
        $table->addColumn('os_family', 'string', ['length' => 15]);
        $table->addColumn('pkg_name', 'string', ['length' => 255]);
        $table->addColumn('preview_url', 'string', ['notnull' => false, 'length' => 1024]);
        $table->addColumn('icon_url', 'string', ['notnull' => false, 'length' => 1024]);
        $table->addColumn('description', 'text', ['notnull' => false, 'length' => 0]);
        $table->addColumn('min_install', 'string', ['notnull' => false, 'length' => 25]);
        $table->addColumn('max_install', 'string', ['notnull' => false, 'length' => 25]);
        $table->addColumn('os_minversion', 'string', ['length' => 55]);
        $table->addColumn('os_maxversion', 'string', ['notnull' => false, 'length' => 55]);
        $table->addColumn('pkg_size', 'string', ['notnull' => false, 'length' => 20]);
        $table->addColumn('created_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->addColumn('updated_at', 'datetime', ['length' => 0, 'comment' => '(DC2Type:datetime)']);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['pkg_name'], 'UNIQ_95F9C90D90169004');
    }

    /**
     * Add oh_advertiser foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhAdvertiserForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_advertiser');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['assigned_to_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['updated_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_business_unit'),
            ['business_unit_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['created_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_advertiser_account foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhAdvertiserAccountForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_advertiser_account');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['updated_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_business_unit'),
            ['business_unit_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['created_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser'),
            ['advertiser_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_click_macro foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhClickMacroForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_click_macro');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_conversion foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhConversionForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_conversion');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
    }

    /**
     * Add oh_media foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhMediaForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_media');
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['updated_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_business_unit'),
            ['business_unit_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_user'),
            ['created_by_user_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_media_blacklist foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhMediaBlacklistForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_media_blacklist');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_media_has_accounts foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhMediaHasAccountsForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_media_has_accounts');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_media_has_offers foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhMediaHasOffersForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_media_has_offers');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_media_postback foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhMediaPostbackForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_media_postback');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_offer foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhOfferForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_offer');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer_target'),
            ['target_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_application'),
            ['application_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oro_business_unit'),
            ['business_unit_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_offer_creative foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhOfferCreativeForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_offer_creative');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_offer_has_countries foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhOfferHasCountriesForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_offer_has_countries');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer_country'),
            ['country_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
    }

    /**
     * Add oh_offer_target foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhOfferTargetForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_offer_target');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer_target'),
            ['parent_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
    }

    /**
     * Add oh_profit_model foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhProfitModelForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_profit_model');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => null]
        );
    }

    /**
     * Add oh_report foreign keys.
     *
     * @param Schema $schema
     * @throws SchemaException
     */
    protected function addOhReportForeignKeys(Schema $schema)
    {
        $table = $schema->getTable('oh_report');
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_offer'),
            ['offer_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser_account'),
            ['account_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_advertiser'),
            ['advertiser_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
        $table->addForeignKeyConstraint(
            $schema->getTable('oh_media'),
            ['media_id'],
            ['id'],
            ['onDelete' => null, 'onUpdate' => null]
        );
    }
}
