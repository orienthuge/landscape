<?php

namespace OrientHuge\CoreBundle\Migrations\Data\Demo\ORM;

use OrientHuge\CoreBundle\Entity\Advertiser;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Oro\Bundle\OrganizationBundle\Migrations\Data\ORM\LoadOrganizationAndBusinessUnitData;
use Oro\Bundle\UserBundle\Entity\UserManager;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadRolesData;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAdvertiserData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    public const ESUNNY = 'esunny';
    public const SENAL = 'senal';
    public const WELLSOURCE = 'wellsource';
    public const MAJORMAX = 'majormax';
    public const IVYWEALTH = 'ivywealth';

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadRolesData::class,
            LoadOrganizationAndBusinessUnitData::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->userManager = $container->get('oro_user.manager');
    }

    public function load(ObjectManager $manager)
    {
        $adminRole = $manager->getRepository('OroUserBundle:Role')
            ->findOneBy(['role' => LoadRolesData::ROLE_ADMINISTRATOR]);

        if (!$adminRole) {
            throw new \RuntimeException('Administrator role should exist.');
        }

        $adminUser = $this->userManager->findUserBy(['id' => 1]);

        $advertisers = [
            self::ESUNNY => ['name' => 'eSunny', 'memo' => 'eSunny Inc.,Ltd', 'site' => 'https://www.esunny.net'],
            self::SENAL => ['name' => 'Senal', 'memo' => 'Senal Inc., Ltd', 'site' => 'https://www.senal.mobi'],
            self::IVYWEALTH => ['name' => 'IvyWealth', 'memo' => 'IvyWealth Investment Ltd.', 'site' => 'https://www.ivywealth.com'],
            self::MAJORMAX => ['name' => 'majormax', 'memo' => 'MajorMax Co., Ltd.', 'site' => 'https://www.majormax.com'],
            self::WELLSOURCE => ['name' => 'wellsource', 'memo' => 'Well Source Co., Ltd.', 'site' => 'https://www.wellsource.com'],
        ];

        foreach ($advertisers as $id=>$item) {
            $advertiser = new Advertiser();
            $advertiser
                ->setName($item['name'])
                ->setMemo($item['memo'])
                ->setWebsite($item['site'])
                ->setCreatedBy($adminUser)
                ->setAssignedTo($adminUser)
            ;
            $manager->persist($advertiser);
            $this->addReference($id, $advertiser);
        }
        $manager->flush();
    }
}
