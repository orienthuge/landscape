<?php

namespace OrientHuge\CoreBundle\Migrations\Data\Demo\ORM;

use OrientHuge\CoreBundle\Entity\Offer;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOfferData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadApplicationData::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $apps = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q"];
        foreach ($apps as $appId) {
            $application = $this->getReference($appId);
            $offer = new Offer();
            $offer
                ->setName($application->getName() . " " . $application->getOsFamily() . " DDL (private)")
                ->setPlatform('google')
                ->setClickUrl('https://m.cocoad.mobi/click?tid={TID}')
                ->setTrafficType('non-incentive')
                ->setBidType('CPA')
                ->setExternalId($appId)
                ->setSourceId($appId)
                ->setUuid($appId)
                ->setStatus('enabled')
            ;
            $application->addOffer($offer);

            $manager->persist($offer);
            $manager->persist($application);
        }
        $manager->flush();
    }
}
