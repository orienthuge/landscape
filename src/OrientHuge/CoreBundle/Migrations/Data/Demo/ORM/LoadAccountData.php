<?php

namespace OrientHuge\CoreBundle\Migrations\Data\Demo\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OrientHuge\CoreBundle\Entity\Account;

class LoadAccountData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadAdvertiserData::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $advertisers = [
            LoadAdvertiserData::ESUNNY,
            LoadAdvertiserData::SENAL,
            LoadAdvertiserData::MAJORMAX,
            LoadAdvertiserData::WELLSOURCE,
            LoadAdvertiserData::IVYWEALTH,
        ];
        foreach ($advertisers as $advertiser) {
            $account = new Account();
            $account
                ->setName($advertiser)
                ->setAdvertiser($this->getReference($advertiser))
                ->setHideReferrer(true)
                ->setStatus('enabled')
                ->setPayoutPercent(0.95)
                ->setDailyBudget(2000)
                ->setDailyCap(2000)
                ->setFetchInterval(200)
            ;
            $manager->persist($account);
        }

        $manager->flush();
    }
}
