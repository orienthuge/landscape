<?php

namespace OrientHuge\CoreBundle\Migrations\Data\Demo\ORM;

use OrientHuge\CoreBundle\Entity\Application;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @property array appList
 */
class LoadApplicationData extends AbstractFixture
{
    public static $AppList = [];

    public function __construct()
    {
        self::$AppList = [
            'A' => [ "name" => 'M88', 'pkg_name' => 'com.m88' ],
            'B' => [ "name" => 'Live Casino', 'pkg_name' => 'com.live_casino' ],
            'C' => [ "name" => 'Spin City', 'pkg_name' => 'com.spin_city' ],
            'D' => [ "name" => 'Unibet Casino', 'pkg_name' => 'com.unibet.casino' ],
            'E' => [ "name" => 'Unibet Poker', 'pkg_name' => 'com.unibet.poker' ],
            'F' => [ "name" => 'Sky Casino', 'pkg_name' => 'com.sky.casino' ],
            'G' => [ "name" => '萌主捕鱼', 'pkg_name' => 'com.fishing' ],
            'H' => [ "name" => 'Texas Mahjong', 'pkg_name' => 'com.texas.mahjong' ],
            'I' => [ "name" => 'bwin Casino', 'pkg_name' => 'com.bwin.casino' ],
            'J' => [ "name" => 'bitfair Exchange', 'pkg_name' => 'com.bitfair.exchange' ],
            'K' => [ "name" => 'SportsBook', 'pkg_name' => 'com.sports.book' ],
            'L' => [ "name" => 'bwin Sports', 'pkg_name' => 'com.bwin.sports' ],
            'M' => [ "name" => 'CrownBet', 'pkg_name' => 'com.crownbet' ],
            'N' => [ "name" => '1XBet', 'pkg_name' => 'com.1xbet' ],
            'O' => [ "name" => 'MacauSlot', 'pkg_name' => 'com.macauslot' ],
            'P' => [ "name" => 'Betway', 'pkg_name' => 'com.betway' ],
            'Q' => [ "name" => 'Pinlite', 'pkg_name' => 'com.pinlite' ],
        ];
    }


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::$AppList as $id => $item) {
            $app = new Application();
            $app
                ->setName($item['name'])
                ->setOsFamily('android')
                ->setPkgName($item['pkg_name'])
                ->setOsMinversion("4")
            ;
            $manager->persist($app);
            $this->addReference($id, $app);
        }
        $manager->flush();
    }
}