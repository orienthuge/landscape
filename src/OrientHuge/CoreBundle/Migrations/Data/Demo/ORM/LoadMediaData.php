<?php

namespace OrientHuge\CoreBundle\Migrations\Data\Demo\ORM;

use OrientHuge\CoreBundle\Entity\Media;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Oro\Bundle\OrganizationBundle\Migrations\Data\ORM\LoadOrganizationAndBusinessUnitData;
use Oro\Bundle\UserBundle\Entity\UserManager;
use Oro\Bundle\UserBundle\Migrations\Data\ORM\LoadRolesData;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadMediaData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            LoadRolesData::class,
            LoadOrganizationAndBusinessUnitData::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->userManager = $container->get('oro_user.manager');
    }

    public function load(ObjectManager $manager)
    {
        $adminRole = $manager->getRepository('OroUserBundle:Role')
            ->findOneBy(['role' => LoadRolesData::ROLE_ADMINISTRATOR]);

        if (!$adminRole) {
            throw new \RuntimeException('Administrator role should exist.');
        }

        $adminUser = $this->userManager->findUserBy(['id' => 1]);

        $media = new Media();
        $media
            ->setName("Lucky Ford")
            ->setCreatedBy($adminUser)
        ;
        $manager->persist($media);

        $media = new Media();
        $media
            ->setName("Lucky Charter")
            ->setCreatedBy($adminUser)
        ;
        $manager->persist($media);
        $manager->flush();
    }
}
