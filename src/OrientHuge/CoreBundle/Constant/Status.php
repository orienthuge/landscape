<?php
/**
 * Created by PhpStorm.
 * User: rick
 * Date: 2017/12/12
 * Time: 19:31
 */
namespace OrientHuge\CoreBundle\Constant;

final class Status
{
    const ENABLED  = 'enabled';
    const DISABLED = 'disabled';
    const PAUSED   = 'paused';
}
