<?php
namespace OrientHuge\CoreBundle\Form\Handler;

use OrientHuge\CoreBundle\Entity\Account;
use OrientHuge\CoreBundle\Entity\Media;
use Symfony\Component\Form\FormInterface;

class AccountHandler extends FormHandler
{
    /**
     * "Success" form handler
     *
     * @param $entity
     * @param FormInterface $form
     */
    protected function onSuccess($entity, FormInterface $form)
    {
        $this->appendMedias($entity, $form->get('appendMedias')->getData());
        $this->removeMedias($entity, $form->get('removeMedias')->getData());
        parent::onSuccess($entity, $form);
    }

    /**
     * Append medias to account
     * @param Account $account
     * @param array $medias
     */
    protected function appendMedias(Account $account, array $medias)
    {
        /** @var $media Media */
        foreach ($medias as $media) {
            $media->addAccount($account);
            $this->manager->persist($media);
        }
    }

    /**
     * Remove medias from account
     *
     * @param Account $account
     * @param Media[] $medias
     */
    protected function removeMedias(Account $account, array $medias)
    {
        /** @var $media Media */
        foreach ($medias as $media) {
            $media->removeAccount($account);
            $this->manager->persist($media);
        }
    }
}
