<?php

namespace OrientHuge\CoreBundle\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;

use OrientHuge\CoreBundle\Entity\Account;
use OrientHuge\CoreBundle\Entity\ClickMacro;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Oro\Bundle\SoapBundle\Entity\Manager\ApiEntityManager;

class MacroHandler
{
    /** @var EntityManagerInterface */
    protected $manager;

    /**
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager )
    {
        $this->manager = $manager;
    }

    /**
     * Process form
     *
     * @param  $entity
     * @param  Form $form
     * @param  Request $request
     * @return bool True on successful processing, false otherwise
     */
    public function process($entity, Form $form, Request $request)
    {
        $form->setData($entity);

//        $submitData = [
//            'field' => $request->request->get('field'),
//            'value' => $request->request->get('value')
//        ];

        $form->handleRequest($request);

        if ($request->request->get('accountId') && $form->isSubmitted() && $form->isValid()) {
            $account = $this->manager->find(
                'OrientHugeCoreBundle:Account',
                $request->request->get('accountId')
            );

            $this->onSuccess($entity, $account);
        }

        return true;
    }

    /**
     * @param $id
     * @param ApiEntityManager $manager
     *
     * @throws \Exception
     */
    public function handleDelete($id, ApiEntityManager $manager)
    {
        /** @var ClickMacro $macro */
        $macro = $manager->find($id);

        $em = $manager->getObjectManager();
        $em->remove($macro);
        $em->flush();
    }

    /**
     * @param ClickMacro $entity
     * @param Account $account
     */
    protected function onSuccess(ClickMacro $entity, Account $account)
    {
        $entity->setAccount($account);
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}
