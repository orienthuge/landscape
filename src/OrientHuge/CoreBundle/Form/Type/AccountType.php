<?php

namespace OrientHuge\CoreBundle\Form\Type;

use OrientHuge\CoreBundle\Entity\Account;
use OrientHuge\CoreBundle\Entity\Advertiser;
use Oro\Bundle\EntityBundle\Provider\EntityNameResolver;
use Oro\Bundle\EntityConfigBundle\Config\Id\EntityConfigId;
use Oro\Bundle\FormBundle\Form\Type\EntityIdentifierType;
use Oro\Bundle\FormBundle\Form\Type\OroMoneyType;
use Oro\Bundle\FormBundle\Form\Type\OroPercentType;
use Oro\Bundle\TranslationBundle\Form\Type\TranslatableEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AccountType extends AbstractType
{
    /** @var RouterInterface */
    protected $router;

    /** @var EntityNameResolver */
    protected $entityNameResolver;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /**
     * @param RouterInterface               $router
     * @param EntityNameResolver            $entityNameResolver
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(
        RouterInterface $router,
        EntityNameResolver $entityNameResolver,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->router = $router;
        $this->entityNameResolver = $entityNameResolver;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildPlainFields($builder, $options);
        $this->buildRelationFields($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    protected function buildPlainFields(FormBuilderInterface $builder, array $options)
    {
        // basic plain fields
        $builder
            ->add('name', TextType::class, ['required' => true, 'label' => 'Name'])
            ->add('status', StatusType::class, ['required' => true, 'label' => 'Status'])
            ->add('clientId', TextType::class, ['required' => false, 'label' => 'Client Id'])
            ->add('token', TextType::class, ['required' => false, 'label' => 'Token'])
            ->add('requestUrl', TextType::class, ['required' => false, 'label' => 'Request Url'])
            ->add('hideReferrer', CheckboxType::class, ['required' => false, 'label' => 'Hide Referrer'])
            ->add('dailyCap', IntegerType::class, ['required' => false, 'label' => 'Daily Capacity'])
            ->add('dailyBudget', OroMoneyType::class, ['required' => false, 'label' => 'Daily Budget'])
            ->add('payoutPercent', OroPercentType::class, ['required' => false, 'label' => 'Payout Percent'])
            ->add(
                'fetchInterval',
                IntegerType::class,
                [
                    'required' => false, 'label' => 'Offer Fetch Interval',
                    'tooltip' => 'fetch offers time interval, default 30 minutes',
                ]
            )
        ;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildRelationFields(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'advertiser',
            TranslatableEntityType::class,
            [
                'label'       => 'Advertiser',
                'class'       => Advertiser::class,
                'required'    => true,
                'empty_value' => 'Choose an advertiser',
            ]
        );
        $builder->add(
            'clickMacros',
            MacroCollectionType::class,
            [
                'label'           => 'Macro',
                'type'            => MacroType::class,
                'required'        => false,
                'intention'       => 'account',
            ]
        );
        $builder->add(
            'appendMedias',
            EntityIdentifierType::class,
            array(
                'class'    => 'OrientHugeCoreBundle:Media',
                'required' => false,
                'mapped'   => false,
                'multiple' => true,
            )
        );
        $builder->add(
            'removeMedias',
            EntityIdentifierType::class,
            array(
                'class'    => 'OrientHugeCoreBundle:Media',
                'required' => false,
                'mapped'   => false,
                'multiple' => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'           => Account::class,
                'intention'            => 'account',
                'config_id'            => new EntityConfigId('extend', Account::class),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oh_account';
    }
}
