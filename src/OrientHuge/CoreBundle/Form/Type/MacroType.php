<?php
namespace OrientHuge\CoreBundle\Form\Type;

use OrientHuge\CoreBundle\Entity\ClickMacro;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MacroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', HiddenType::class);
        $builder->add('field', TextType::class, ['required' => true, 'label' => false, 'attr' => ['placeholder' => 'Field']]);
        $builder->add('value', TextType::class, ['required' => true, 'label' => false, 'attr' => ['placeholder' => 'Value']]);
        $builder->add('android', CheckboxType::class, ['required' => false, 'label' => false]);
        $builder->add('ios', CheckboxType::class, ['required'=> false, 'label' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'           => ClickMacro::class,
                'csrf_protection'      => false,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oh_macro';
    }
}
