<?php

namespace OrientHuge\CoreBundle\Form\Type;

use OrientHuge\CoreBundle\Entity\Media;
use Oro\Bundle\EntityConfigBundle\Config\Id\EntityConfigId;
use Oro\Bundle\FormBundle\Form\Type\OroMoneyType;
//use Oro\Bundle\UserBundle\Form\Type\OrganizationUserAclSelectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildPlainFields($builder, $options);
        $this->buildRelationFields($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    protected function buildPlainFields(FormBuilderInterface $builder, array $options)
    {
        // basic plain fields
        $builder
            ->add('name', TextType::class, ['required' => true, 'label' => 'Name'])
            ->add('status', StatusType::class, ['required' => true, 'label' => 'Status'])
            ->add(
                'clientId',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'Client Id',
                    'attr' => ['readonly' => true, 'placeholder' => 'generated automatically']
                ]
            )
            ->add(
                'token',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'Token',
                    'attr' => ['readonly' => true, 'placeholder' => 'generated automatically']
                ]
            )
            ->add('dailyCap', IntegerType::class, ['required' => false, 'label' => 'Daily Capacity'])
            ->add('dailyCredit', OroMoneyType::class, ['required' => false, 'label' => 'Daily Credit'])
            ->add('creditLevel', IntegerType::class, ['required' => false, 'label' => 'Credit Level'])
        ;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function buildRelationFields(FormBuilderInterface $builder, array $options)
    {
        // assigned to (user)
//        $builder->add(
//            'assignedTo',
//            OrganizationUserAclSelectType::class,
//            ['required' => true, 'label' => 'Assigned To']
//        );
        $builder->add(
            'blacklists',
            BlacklistCollectionType::class,
            [
                'label'           => 'Blacklists',
                'type'            => BlacklistType::class,
                'required'        => false,
                'intention'       => 'media',
            ]
        );
        $builder->add(
            'postbacks',
            PostbackCollectionType::class,
            [
                'label'           => 'Postback Lists',
                'type'            => PostbackType::class,
                'required'        => false,
                'intention'       => 'media',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'           => Media::class,
                'intention'            => 'media',
                'config_id'            => new EntityConfigId('extend', Media::class),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'oh_media';
    }
}
