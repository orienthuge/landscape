<?php

namespace OrientHuge\CoreBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;

/**
 * Class MediaController
 *
 * @Route("/publisher")
 * @package OrientHuge\CoreBundle\Controller
 */
class MediaController extends Controller
{
    /**
     * @Route("/{_format}", name="oh_media_index", requirements={"_format"="html|json"}, defaults={"_format"="html"})
     * @AclAncestor("oh_media_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Media::class];
    }

    /**
     * @Route("/view/{id}", name="oh_media_view", requirements={"id"="\d+"})
     * @Acl(
     *      id="oh_media_view",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Media",
     *      permission="VIEW"
     * )
     * @Template()
     * @param Media $media
     * @return array
     */
    public function viewAction(Media $media)
    {
        return ['entity' => $media];
    }

    /**
     * @Route("/create", name="oh_media_create")
     * @Acl(
     *      id="oh_media_create",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Media",
     *      permission="CREATE"
     * )
     * @Template("OrientHugeCoreBundle:Media:update.html.twig")
     */
    public function createAction()
    {
        return $this->update(new Media());
    }

    /**
     * @Route("/update/{id}", name="oh_media_update", requirements={"id"="\d+"})
     * @Acl(
     *      id="oh_media_update",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Media",
     *      permission="EDIT"
     * )
     * @Template("OrientHugeCoreBundle:Media:update.html.twig")
     * @param Media $entity
     * @return array
     */
    public function updateAction(Media $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Media $entity
     * @return array
     */
    protected function update(Media $entity = null)
    {
        return $this->get('oro_form.update_handler')->update(
            $entity,
            $this->get('oh.form.media'),
            'Success! Media created!',
            null,
            'oh_form_handler',
            null
        );
    }

    /**
     * @Route("/widget/info/{id}", name="oh_media_widget_info", requirements={"id"="\d+"})
     * @AclAncestor("oh_media_view")
     * @Template("OrientHugeCoreBundle:Media/widget:info.html.twig")
     * @param Media $media
     * @return array
     */
    public function infoAction(Media $media)
    {
        return ['media' => $media];
    }

    /**
     * @Route(
     *      "/widget/blacklists/{id}",
     *      name="oh_media_widget_blacklists",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_media_view")
     * @Template("OrientHugeCoreBundle:Media/widget:blacklists.html.twig")
     * @param Media|null $media
     * @return array
     */
    public function blacklistsAction(Media $media = null)
    {
        return ['entity' => $media];
    }

    /**
     * @Route(
     *      "/widget/postbacks/{id}",
     *      name="oh_media_widget_postbacks",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_media_view")
     * @Template("OrientHugeCoreBundle:Media/widget:postbacks.html.twig")
     * @param Media|null $media
     * @return array
     */
    public function postbacksAction(Media $media = null)
    {
        return ['entity' => $media];
    }

    /**
     * @Route(
     *      "/widget/offers/{id}",
     *      name="oh_media_widget_offers",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_media_view")
     * @Template("OrientHugeCoreBundle:Media/widget:offers.html.twig")
     * @param Media|null $media
     * @return array
     */
    public function offersAction(Media $media = null)
    {
        return ['entity' => $media];
    }
}
