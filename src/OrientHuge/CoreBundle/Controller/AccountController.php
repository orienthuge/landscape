<?php

namespace OrientHuge\CoreBundle\Controller;

use OrientHuge\CoreBundle\Entity\Account;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class AdvertiserController
 *
 * @Route("/advertiser/account")
 * @package OrientHuge\CoreBundle\Controller
 */
class AccountController extends Controller
{
    /**
     * @Route("/", name="oh_account_index")
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Account::class];
    }

    /**
     * @Route("/view/{id}", name="oh_account_view", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     * @param Account $account
     * @return array
     */
    public function viewAction(Account $account)
    {
        return ['entity' => $account];
    }

    /**
     * @Route("/create", name="oh_account_create")
     * @AclAncestor("oh_advertiser_create")
     * @Template("OrientHugeCoreBundle:Account:update.html.twig")
     */
    public function createAction()
    {
        return $this->update(new Account());
    }

    /**
     * @Route("/update/{id}", name="oh_account_update")
     * @AclAncestor("oh_advertiser_update")
     * @Template
     * @param Account $entity
     * @return array
     */
    public function updateAction(Account $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Account $entity
     * @return array
     * @throws BadRequestHttpException
     */
    protected function update(Account $entity)
    {
        return $this->get('oro_form.update_handler')->update(
            $entity,
            $this->get('oh.form.account'),
            'Success! Account created!',
            null,
            'oh_account_handler',
            null
        );
    }

    /**
     * @Route("/widget/info/{id}", name="oh_account_widget_info", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeCoreBundle:Account/widget:info.html.twig")
     * @param Account $account
     * @return array
     */
    public function infoAction(Account $account)
    {
        return ['account' => $account];
    }
}
