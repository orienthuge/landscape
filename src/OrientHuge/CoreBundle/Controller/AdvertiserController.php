<?php

namespace OrientHuge\CoreBundle\Controller;

use OrientHuge\CoreBundle\Entity\Advertiser;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;

/**
 * Class AdvertiserController
 *
 * @Route("/advertiser")
 * @package OrientHuge\CoreBundle\Controller
 */
class AdvertiserController extends Controller
{
    /**
     * @Route("/{_format}", name="oh_advertiser_index", requirements={"_format"="html|json"}, defaults={"_format"="html"})
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Advertiser::class];
    }

    /**
     * @Route("/view/{id}", name="oh_advertiser_view", requirements={"id"="\d+"})
     * @Acl(
     *      id="oh_advertiser_view",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Advertiser",
     *      permission="VIEW"
     * )
     * @Template()
     * @param Advertiser $advertiser
     * @return array
     */
    public function viewAction(Advertiser $advertiser)
    {
        return ['entity' => $advertiser];
    }

    /**
     * @Route("/create", name="oh_advertiser_create")
     * @Acl(
     *      id="oh_advertiser_create",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Advertiser",
     *      permission="CREATE"
     * )
     * @Template("OrientHugeCoreBundle:Advertiser:update.html.twig")
     */
    public function createAction()
    {
        return $this->update(new Advertiser());
    }

    /**
     * @Route("/update/{id}", name="oh_advertiser_update", requirements={"id"="\d+"})
     * @Acl(
     *      id="oh_advertiser_update",
     *      type="entity",
     *      class="OrientHugeCoreBundle:Advertiser",
     *      permission="EDIT"
     * )
     * @Template("OrientHugeCoreBundle:Advertiser:update.html.twig")
     * @param Advertiser $entity
     * @return array
     */
    public function updateAction(Advertiser $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Advertiser $entity
     * @return array
     */
    protected function update(Advertiser $entity)
    {
        return $this->get('oro_form.update_handler')->update(
            $entity,
            $this->get('oh.form.advertiser'),
            'Success! Advertiser created/updated!',
            null,
            'oh_form_handler',
            null
        );
    }

    /**
     * @Route(
     *      "/widget/accounts/{id}",
     *      name="oh_advertiser_widget_accounts",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=0}
     * )
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeCoreBundle:Advertiser/widget:accounts.html.twig")
     * @param Advertiser|null $advertiser
     * @return array
     */
    public function accountsAction(Advertiser $advertiser = null)
    {
        return [
            'entity' => $advertiser
        ];
    }

    /**
     * @Route("/widget/info/{id}", name="oh_advertiser_widget_info", requirements={"id"="\d+"})
     * @AclAncestor("oh_advertiser_view")
     * @Template("OrientHugeCoreBundle:Advertiser/widget:info.html.twig")
     * @param Advertiser $advertiser
     * @return array
     */
    public function infoAction(Advertiser $advertiser)
    {
        return ['advertiser' => $advertiser];
    }
}
