<?php

namespace OrientHuge\CoreBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\Blacklist;
use OrientHuge\CoreBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class BlacklistController
 *
 * @Route("/media")
 * @package OrientHuge\CoreBundle\Controller
 */
class BlacklistController extends Controller
{
    /**
     * @Route("/blacklist/{_format}", name="oh_blacklist_index", requirements={"_format"="html|json"}, defaults={"_format"="html"})
     * @AclAncestor("oh_media_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => Blacklist::class];
    }

    /**
     * @Route("/{mediaId}/blacklist/create", name="oh_blacklist_create", requirements={"mediaId"="\d+"})
     * @AclAncestor("oh_media_create")
     * @Template("OrientHugeCoreBundle:Blacklist:update.html.twig")
     * @param Media $media
     * @return array
     */
    public function createAction(Media $media)
    {
        return $this->update(new Blacklist($media));
    }

    /**
     * @Route("/blacklist/update/{id}", name="oh_blacklist_update", requirements={"id"="\d+"})
     * @AclAncestor("oh_media_update")
     * @Template("OrientHugeCoreBundle:Blacklist:update.html.twig")
     * @param Blacklist $entity
     * @return array
     */
    public function updateAction(Blacklist $entity)
    {
        return $this->update($entity);
    }

    /**
     * @param Blacklist $entity
     * @return array
     */
    protected function update(Blacklist $entity = null)
    {
        return $this->get('oro_form.update_handler')->update(
            $entity,
            $this->get('oh.form.blacklist'),
            'Success! Blacklist created/updated!',
            null,
            'oh_form_handler',
            null
        );
    }

    /**
     * @Route("/{id}/blacklist", name="oh_media_blacklists", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("oh_media_view")
     * @param Media $media
     * @return array
     */
    public function blacklistAction(Media $media)
    {
        return array(
            'entity' => $media,
            'blacklist_edit_acl_resource' => 'oh_media_update'
        );
    }
}
