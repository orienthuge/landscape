<?php

namespace OrientHuge\CoreBundle\Controller\Api\Rest;

use OrientHuge\CoreBundle\Entity\Advertiser;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Routing\ClassResourceInterface;

use Oro\Bundle\SoapBundle\Entity\Manager\ApiEntityManager;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\SoapBundle\Form\Handler\ApiFormHandler;
use Oro\Bundle\SoapBundle\Controller\Api\Rest\RestController;
use Oro\Bundle\ChannelBundle\Provider\Lifetime\AmountProvider;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @RouteResource("advertiser")
 * @NamePrefix("oh_api_")
 */
class AdvertiserController extends RestController implements ClassResourceInterface
{
  /**
   * REST GET list
   *
   * @QueryParam(
   *      name="page",
   *      requirements="\d+",
   *      nullable=true,
   *      description="Page number, starting from 1. Defaults to 1."
   * )
   * @QueryParam(
   *      name="limit",
   *      requirements="\d+",
   *      nullable=true,
   *      description="Number of items per page. defaults to 10."
   * )
   * @ApiDoc(
   *      description="Get all advertiser items",
   *      resource=true
   * )
   * @AclAncestor("oh_advertiser_view")
   * @param Request $request
   * @return Response
   */
    public function cgetAction(Request $request)
    {
        $page = (int)$request->get('page', 1);
        $limit = (int)$request->get('limit', self::ITEMS_PER_PAGE);

        return $this->handleGetListRequest($page, $limit);
    }

    /**
     * REST GET item
     *
     * @param string $id
     *
     * @ApiDoc(
     *      description="Get advertiser item",
     *      resource=true
     * )
     * @AclAncestor("oh_advertiser_view")
     * @return Response
     */
    public function getAction($id)
    {
        return $this->handleGetRequest($id);
    }

    /**
     * REST PUT
     *
     * @param int $id Advertiser item id
     *
     * @ApiDoc(
     *      description="Update advertiser",
     *      resource=true
     * )
     * @AclAncestor("oh_advertiser_update")
     * @return Response
     */
    public function putAction($id)
    {
        return $this->handleUpdateRequest($id);
    }

    /**
     * Create new advertiser
     *
     * @ApiDoc(
     *      description="Create new advertiser",
     *      resource=true
     * )
     * @AclAncestor("oh_advertiser_create")
     */
    public function postAction()
    {
        return $this->handleCreateRequest();
    }

    /**
     * REST DELETE
     *
     * @param int $id
     *
     * @ApiDoc(
     *      description="Delete Advertiser",
     *      resource=true
     * )
     * @Acl(
     *      id="oh_advertiser_delete",
     *      type="entity",
     *      permission="DELETE",
     *      class="OrientHugeCoreBundle:Advertiser"
     * )
     * @return Response
     */
    public function deleteAction($id)
    {
        return $this->handleDeleteRequest($id);
    }

    /**
     * Get entity Manager
     *
     * @return ApiEntityManager
     */
    public function getManager()
    {
        return $this->get('oh.advertiser.manager.api');
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->get('oh.form.advertiser.api');
    }

    /**
     * @return ApiFormHandler
     */
    public function getFormHandler()
    {
        return $this->get('oh.form.handler.advertiser.api');
    }

    protected function getPreparedItem($entity, $resultFields = [])
    {
        $result = parent::getPreparedItem($entity, $resultFields);

        /** @var AmountProvider $amountProvider  */
        $amountProvider = $this->get('oro_channel.provider.lifetime.amount_provider');

        $result['lifetimeValue'] = $amountProvider->getAccountLifeTimeValue($entity);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function getPreparedItems($entities, $resultFields = [])
    {
        $result = [];
        $ids = array_map(
            function (Advertiser $advertiser) {
                return $advertiser->getId();
            },
            $entities
        );

        $ap = $this->get('oro_channel.provider.lifetime.amount_provider');
        $lifetimeValues = $ap->getAccountsLifetimeQueryBuilder($ids)
            ->getQuery()
            ->getArrayResult();
        $lifetimeMap = [];
        foreach ($lifetimeValues as $value) {
            $lifetimeMap[$value['accountId']] = (float)$value['lifetimeValue'];
        }

        foreach ($entities as $entity) {
            /** @var Advertiser $entity */
            $entityArray = parent::getPreparedItem($entity, $resultFields);
            if (array_key_exists($entity->getId(), $lifetimeMap)) {
                $entityArray['lifetimeValue'] = $lifetimeMap[$entity->getId()];
            } else {
                $entityArray['lifetimeValue'] = 0.0;
            }

            $result[] = $entityArray;
        }

        return $result;
    }
}
