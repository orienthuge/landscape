<?php

namespace OrientHuge\CoreBundle\Controller;

use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use OrientHuge\CoreBundle\Entity\ProfitModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class ProfitModelController
 *
 * @Route("/profit-model")
 * @package OrientHuge\CoreBundle\Controller
 */
class ProfitModelController extends Controller
{
    /**
     * @Route("/", name="oh_profit_model_index")
     * @AclAncestor("oh_advertiser_view")
     * @Template()
     */
    public function indexAction()
    {
        return ['entity_class' => ProfitModel::class];
    }
}
