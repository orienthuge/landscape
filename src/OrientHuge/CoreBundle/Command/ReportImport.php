<?php
namespace OrientHuge\CoreBundle\Command;

use DateTime;
use Exception;
use Doctrine\ORM\EntityManagerInterface;
use OrientHuge\CoreBundle\Entity\Offer;
use OrientHuge\CoreBundle\Entity\Media;
use OrientHuge\CoreBundle\Entity\Account;
use OrientHuge\CoreBundle\Entity\AdReport;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReportImport extends Command
{
    protected $em;
    protected $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        parent::__construct('orienthuge:report:import');
        $this->em = $em;
        $this->logger = $logger;
    }

    protected function configure()
    {
        parent::configure(); // TODO: Change the autogenerated stub
        $this
            ->setDescription('import report csv to database. orienthuge:report:import csv --data=')
            ->addArgument('csv', null, InputArgument::REQUIRED)
            ->addOption('data', null, InputOption::VALUE_REQUIRED)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $line = $input->getArgument('csv');
        if (!$line) {
            $this->logger->error('should use csv as command argument');
            return false;
        }
        $data = $input->getOption('data');
        $item = str_getcsv($data);
        $output->writeln($data);
        // csv data format: [
        // 0 => date,
        // 1 => offer external id,
        // 2 => publisher client id,
        // 3 => advertiser account name,
        // 4 => impression,
        // 5 => click,
        // 6 => conversion,
        // 7 => cpm,
        // 8 => price
        // 9 => revenue
        // ]
        if ($item && count($item) != 10) {
            $output->writeln('<error>item count is not equal to 9.</error>');
            return false;
        }

        $report = new AdReport();

        $offer = $this->em->getRepository(Offer::class)->findOneBy(['externalId' => $item[1]]);
        $media = $this->em->getRepository(Media::class)->findOneBy(['clientId' => $item[2]]);
        $account = $this->em->getRepository(Account::class)->findOneBy(['name' => $item[3]]);
        if (!($media && $account && $offer)) {
            $output->writeln('<error>at least one of media,account,offer is null</error>');
            return false;
        }
        $advertiser = $account->getAdvertiser();

        $report
            ->setDate(new DateTime($item[0]))
            ->setImpression($item[4])
            ->setClick($item[5])
            ->setConversion($item[6])
            ->setCpm($item[7])
            ->setPrice($item[8])
            ->setOffer($offer)
            ->setMedia($media)
            ->setAccount($account)
            ->setAdvertiser($advertiser)
            ->setRevenue($item[9])
        ;

        $this->em->persist($report);
        $this->em->flush();
        $output->writeln('<success>done</success>');
    }
}