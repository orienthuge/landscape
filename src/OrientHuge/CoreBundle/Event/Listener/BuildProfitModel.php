<?php
namespace OrientHuge\CoreBundle\Event\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use OrientHuge\CoreBundle\Entity\Account;
use OrientHuge\CoreBundle\Entity\Media;
use OrientHuge\CoreBundle\Entity\ProfitModel;

class BuildProfitModel implements EventSubscriber
{
    protected $models = [];

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postFlush'
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        switch (true) {
            case ($entity instanceof Media):
                $this->addMediaProfitModel($em, $entity);
                break;
            case ($entity instanceof Account):
                $this->addAccountProfitModel($em, $entity);
                break;
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (!empty($this->models)) {
            $em = $args->getEntityManager();
            foreach ($this->models as $model) {
                $em->persist($model);
            }
            $this->models = [];
            $em->flush();
        }
    }

    /**
     * @param EntityManager $em
     * @param $entity
     */
    protected function addAccountProfitModel(EntityManager $em, $entity)
    {
        $medias = $em->getRepository(Media::class)->findAll();
        foreach ($medias as $media) {
            $model = new ProfitModel();
            $model->setAccount($entity);
            $model->setMedia($media);
            $this->models[] = $model;
        }
    }

    /**
     * @param EntityManager $em
     * @param $entity
     */
    protected function addMediaProfitModel(EntityManager $em, $entity)
    {
        $accounts = $em->getRepository(Account::class)->findAll();
        foreach ($accounts as $account) {
            $model = new ProfitModel();
            $model->setMedia($entity);
            $model->setAccount($account);
            $this->models[] = $model;
        }
    }
}
